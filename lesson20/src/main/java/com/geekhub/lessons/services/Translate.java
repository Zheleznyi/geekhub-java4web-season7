package com.geekhub.lessons.services;

import com.geekhub.lessons.dto.Alphabet;

import java.util.Map;

public class Translate {

    Map<String, String> map;

    public Translate(Alphabet alphabet) {
        this.map = alphabet.getMap();
    }

    public String translate(String word) {
        word = word.toLowerCase();
        char[] chars = word.toCharArray();
        String translate = "";
        for (char a : chars) {
            if (a == ' ') {
                translate = translate + " /";
            } else {
                Character character = a;
                if (map.get(character.toString()) == null) {
                    throw new IllegalArgumentException("Pls input new data");
                } else {
                    translate = translate + " " + map.get(character.toString());
                }
            }
        }
        return translate;
    }
}
