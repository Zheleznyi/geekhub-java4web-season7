package com.geekhub.lessons.controller;

import com.geekhub.lessons.dto.Alphabet;
import com.geekhub.lessons.services.Translate;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@org.springframework.stereotype.Controller
public class Controller {

    @GetMapping(value = "main")
    public String main(@RequestParam(required = false) String word, Model model){
        Translate translate = new Translate(new Alphabet());
        if(word==null){
            word="";
        }
        String result = translate.translate(word);
        model.addAttribute("result", result);
        return "main";
    }

}