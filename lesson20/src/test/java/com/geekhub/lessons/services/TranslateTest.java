package com.geekhub.lessons.services;

import com.geekhub.lessons.dto.Alphabet;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;


public class TranslateTest {

    private Translate translate;

    @BeforeMethod
    public void setUp(){
        translate = new Translate(new Alphabet());
    }

    @Test
    public void testTranslateSOS(){
        String actualResult = translate.translate("SOS");
        String expectedResult = " ... --- ...";

        assertEquals(actualResult,expectedResult);
    }

    @Test
    public void testTranslateSos(){
        String actualResult = translate.translate("S O S");
        String expectedResult = " ... / --- / ...";

        assertEquals(actualResult,expectedResult);
    }

    @Test
    public void testTranslateGeekHub(){
        String actualResult = translate.translate("GeekHub");
        String expectedResult = " --. . . -.- .... ..- -...";

        assertEquals(actualResult,expectedResult);
    }

    @Test
    public void testTranslateItIsGoodDay(){
        String actualResult = translate.translate("It is a good day to die");
        String expectedResult = " .. - / .. ... / .- / --. --- --- -.. / -.. .- -.-- / - --- / -.. .. .";

        assertEquals(actualResult,expectedResult);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void  testTranslateException(){
        translate.translate("$0m*th!#g Br0k*#!");
    }

}
