package task4;

public class SolvingQuadraticEquations {

    void solve(int discriminant, int a, int b, int c) {

        if (discriminant > 0) {
            int x1 = (int) ((-b - Math.sqrt(discriminant)) / (2 * a));
            int x2 = (int) ((-b + Math.sqrt(discriminant)) / (2 * a));
            System.out.println("Equation have two roots: " + x1 + " and " + x2);
        } else if (discriminant == 0) {
            int x1 = -b / (2 * a);
            System.out.println("Equation have one root: " + x1);
        } else {
            System.out.println("Equation do not have roots");
        }
    }

    int discriminant(int a, int b, int c){
        System.out.println("Equation is " + a + "*x^2+" + b + "*x+" + c + "=0");
        double discriminant = (Math.pow(b, 2) - 4 * a * c);
        if (discriminant%1==0){
            return (int) discriminant;
        } else {
            System.out.println("Pls reenter numbers, discriminant not integer");
            return 0;
        }
    }
}
