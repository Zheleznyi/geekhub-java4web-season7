package task4;

public class Solving {
    public static void main(String[] args) {
        SolvingQuadraticEquations sqe = new SolvingQuadraticEquations();
        sqe.solve(sqe.discriminant(2,7,2),2,7,2);
        sqe.solve(sqe.discriminant(1,2,1),1,2,1);
        sqe.solve(sqe.discriminant(3,1,3),3,1,3);
    }
}
