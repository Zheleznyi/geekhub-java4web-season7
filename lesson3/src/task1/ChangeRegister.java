package task1;

import java.util.Scanner;

public class ChangeRegister {

    private String word;

    String takeWord() {
        System.out.println("Pls, enter word.");
        Scanner in = new Scanner(System.in);
        return in.next();
    }

    void setWord(String wordname) {
        word = wordname;
    }

    boolean identify() {
        char firstSymbol = word.charAt(0);
        return Character.isUpperCase(firstSymbol);
    }

    void changeCase(boolean isUpper) {
        if (isUpper) {
            word = word.toLowerCase();
        } else {
            word = word.toUpperCase();
        }
    }
    void printWord(){
        System.out.println("Changed word is " + word);
    }
}
