package task1;

public class Demo {
    public static void main(String[] args) {
        ChangeRegister changeRegister = new ChangeRegister();
        changeRegister.setWord(changeRegister.takeWord());
        changeRegister.changeCase(changeRegister.identify());
        changeRegister.printWord();
    }
}
