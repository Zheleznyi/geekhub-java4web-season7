package task2;

public class TestStrings {

    void testStr() {
        long time = System.currentTimeMillis();
        String string = "This test of the concathenation methods String, StringBuffer and StringBuilder";
        System.out.println(string);
        for (int i = 0; i < 10000; i++) {
            string = string.concat("+");
        }
        System.out.println(string);
        System.out.println("Time to work with String = " + (System.currentTimeMillis() - time));

    }

    void testStrBuil() {
        long time = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder("This test of the concathenation methods String, " +
                "StringBuffer and StringBuilder");
        System.out.println(sb);
        for (int i = 0; i < 10000; i++) {
            sb.append("+");

        }
        System.out.println(sb);
        System.out.println("Time to create and output StringBuilder = " + (System.currentTimeMillis() - time));

    }

    void testStrBuff() {
        long time = System.currentTimeMillis();
        StringBuffer sbf = new StringBuffer("This test of the concathenation methods String, " +
                "StringBuffer and StringBuilder");
        System.out.println(sbf);
        for (int i = 0; i < 10000; i++) {
            sbf.append("+");

        }
        System.out.println(sbf);
        System.out.println("Time to create and output StringBuffer = " + (System.currentTimeMillis() - time));

    }


}
