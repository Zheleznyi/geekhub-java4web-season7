package task3;

public class CharacteristicsPC {

    public static void main(String[] args) {
        System.out.println("Characteristics PC: Processor: " + System.getenv("PROCESSOR_IDENTIFIER"));
        System.out.println("Architecture: " + System.getenv("PROCESSOR_ARCHITECTURE"));
        System.out.println("Number of processor: " + System.getenv("NUMBER_OF_PROCESSORS"));
        System.out.println("Free memory: " + Runtime.getRuntime().freeMemory() / 1024 / 1024 + " MB");
        System.out.println("Total memory: " + Runtime.getRuntime().totalMemory() / 1024 / 1024 + " MB");
        System.out.println("Max memory: " + Runtime.getRuntime().maxMemory() / 1024 / 1024 + " MB");

    }

}
