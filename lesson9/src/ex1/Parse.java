package ex1;

public class Parse {


    public static int parseInt(String str) throws NumberFormatExeption {
        int number = 0;
        int size;
        char[] array = str.toCharArray();
        size = array.length;
        for (char anArray : array) {
            int n = (int) Math.pow(10, size - 1);
            if (anArray - 48 > 9 || array[0] - 48 == 0 || str.equals(" ")) {
                throw new NumberFormatExeption("Wrong format");
            }
            number = number + (anArray - 48) * n;
            size--;
        }
        return number;
    }
}
