package ex1;

import java.text.ParseException;

public class NumberFormatExeption extends ParseException {
    NumberFormatExeption(String message){
        super(message, 1);
    }
}
