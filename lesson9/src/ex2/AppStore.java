package ex2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class AppStore {
    private Collection<User> users;

    AppStore() {
        this.users = new ArrayList<>();
    }

    void addUser(User user) {
        Objects.requireNonNull(user);
        this.users.add(user);
    }

    public Collection<User> getUsers() {
        return users;
    }


}
