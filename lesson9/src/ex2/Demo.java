package ex2;


public class Demo {
    public static void main(String[] args) {
        User user = new User("com.user", "0");
        User user1 = new User("User1", "1");
        User user2 = new User("User2", "2");
        User user3 = new User("User3", "3");
        AppStore appStore = new AppStore();
        appStore.addUser(user);
        appStore.addUser(user1);
        Auth auth = new Auth();
        try {
            System.out.println(auth.userAuth(user1.getLogin(),user1.getPassword()).getLogin());
            auth.userAuth(user2.getLogin(),user2.getPassword());
            auth.userAuth(user3.getLogin(),user3.getPassword());
            auth.userAuth("","");
            auth.userAuth("com.user","123");


        } catch (UserNotFoundException | WrongCredentialsException u) {
            u.printStackTrace();
        }

    }

}
