package ex2;

class   WrongCredentialsException extends AuthException {

    WrongCredentialsException(String message) {
        super(message);
    }
}
