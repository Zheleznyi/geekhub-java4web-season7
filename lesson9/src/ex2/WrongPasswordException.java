package ex2;

class WrongPasswordException extends WrongCredentialsException {

    WrongPasswordException(String message){
        super(message);
    }
}
