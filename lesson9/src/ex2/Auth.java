package ex2;

import java.util.Collection;

public class Auth {
    User userAuth(String login, String password) throws UserNotFoundException, WrongCredentialsException,
            WrongPasswordException {
        if (login.isEmpty()||password.isEmpty()){
            throw new WrongCredentialsException("Login or password is empty");
        }
        AppStore appStore = new AppStore();
        Collection<User> users = appStore.getUsers();
        for (User user : users) {
            if (user.getLogin().equals(login) && user.getPassword().equals(password)) {
                return user;
            }
            if (user.getLogin().equals(login) && !user.getPassword().equals(password)){
                throw new WrongPasswordException("Wrong password");
            }
        }
        throw new UserNotFoundException("Can not find com.user!!");
    }
}
