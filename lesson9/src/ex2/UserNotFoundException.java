package ex2;

class UserNotFoundException extends AuthException {

    UserNotFoundException(String message){
        super(message);

    }

}

