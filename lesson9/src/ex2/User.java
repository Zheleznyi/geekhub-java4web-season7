package ex2;

public class User {
    private final String login;
    private final String password;


    User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    String getLogin() {
        return login;
    }

    String getPassword() {
        return password;
    }


}
