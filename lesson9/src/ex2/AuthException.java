package ex2;

public class AuthException extends Exception {
    AuthException(String message){
        super(message);
    }
}
