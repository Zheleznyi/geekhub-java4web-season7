import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@WebServlet(name = "homeServlet", urlPatterns = "/home")
public class HomeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> map;
        if ((req.getSession().getAttribute("map") == null)) {
            map = new HashMap<>();
        } else {
            map = (Map<String, String>) req.getSession().getAttribute("map");
        }
        String oname = "" + req.getParameter("name");
        String name = "";
        String value = req.getParameter("value");
        if (oname.contains("=")) {
            char[] array = oname.toCharArray();
            for (int c = 0; c < oname.indexOf("="); c++) {
                name = name + array[c];
            }
        } else {
            name = oname;
        }
        String newval = "";
        String s = map.get(name);
        String gp = "" + req.getParameter("action");
        Map<String, String> mapEnd = iffun(gp, s, newval, req, map, value, name);
        req.getSession().setAttribute("map", mapEnd);
        req.setAttribute("map", mapEnd);
        req.getRequestDispatcher("/WEB-INF/home.jsp").forward(req, resp);
    }

    Map<String, String> iffun(String gp, String s, String newval, HttpServletRequest req, Map<String, String> map,
                      String value, String name) {
        if (gp.equals("delete")) {
            if (s.contains("=")) {
                while (s.contains("=")) {
                    int i = s.indexOf("=");
                    char[] arr = s.toCharArray();
                    for (int b = 0; b < i; b++) {
                        newval = newval + arr[b];
                    }
                    s = s.substring(i + 1);
                    s = s + "w";
                    if (newval.equals(value)) {
                        map.put(name, s);
                    }
                }
            } else {
                map.remove(name);
            }
        } else {
            if (req.getParameter("name") == null || req.getParameter("name").equals("")) {
                throw new RuntimeException();
            } else if (map.containsKey(name)) {
                value = value + "=" + map.get(name);
                map.put(name, value);
            } else {
                map.put(name, value);
            }
        }
        return map;
    }
}
