<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Home</title>
</head>
<body>
<table border="1">
    <tr>
        <td>Name</td>
        <td>Value</td>
        <td>Action</td>
    </tr>
    <c:forEach var="com.user" items="${map}">
        <tr>
        <c:set var="i" value="1"/>
        <c:forTokens items="${com.user}" delims="=" var="name">
            <c:if test="${i>2}">
                <tr>
                    <td></td>
                    <td><c:out value="${name}"/></td>
                    <td><p><a href="/home?action=delete&name=${com.user}&value=${name}">delete</a></p></td>
                </tr>
            </c:if>
            <c:if test="${i<3}">
                <td><c:out value="${name}"/></td>
                <c:if test="${i>1}">
                    <td><p><a href="/home?action=delete&name=${com.user}&value=${name}">delete</a></p></td>
                </c:if>
            </c:if>

            <c:set var="i" value="${i+1}"/>
        </c:forTokens>
        </tr>
    </c:forEach>
    <form action="/home">
        <tr>
            <td><input type="text" name="name"></td>
            <td><input type="text" name="value"></td>
            <td><input type="submit" value="Add"></td>
        </tr>
    </form>
</table>
</body>
</html>
