package ex4;

import java.util.HashSet;
import java.util.Set;

public class SetOperations implements SetOperat{

    public boolean equals(Set a, Set b) {
        if (a.containsAll(b)) {
            return true;
        }
        return false;
    }

    public Set union(Set a, Set b) {
        Set datau = new HashSet();
        datau.addAll(b);
        datau.addAll(a);
        return datau;
    }

    public Set substract(Set a, Set b) {
        Set datau = new HashSet();
        for (Object o : a) {
            if (b.contains(o)) {
            } else {
                datau.add(o);
            }
        }
        return datau;
    }

    public Set intersect(Set a, Set b) {
        Set datau = new HashSet();
        for (Object o : b) {
            if (a.contains(o)) {
                datau.add(o);
            }
        }
        return datau;
    }

    public Set symetricSybstract(Set a, Set b) {
        Set datau = new HashSet();
        for (Object o : b) {
            if (a.contains(o)) {
            } else {
                datau.add(o);
            }
        }
        for (Object o : a) {
            if (b.contains(o)) {
            } else {
                datau.add(o);
            }

        }
        return datau;
    }


}
