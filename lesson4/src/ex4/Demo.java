package ex4;

import java.util.HashSet;
import java.util.Set;

public class Demo {
    public static void main(String[] args) {
        Set set1 = new HashSet();
        Set set2 = new HashSet();
        set1.add("Vasya");
        set1.add("Petya");
        set1.add("Jora");
        set1.add("Vova");
        set2.add("Vasya");
        set2.add("Petya");
        set2.add("Jora");
        set2.add("Vovan");

        SetOperations setop = new SetOperations();


        System.out.println("a equals b  is: " + setop.equals(set1,set2));
        for (Object s : setop.union(set1,set2)) {
            System.out.print(s + ", ");
        }
        System.out.println();
        for (Object s : setop.substract(set1,set2)) {
            System.out.print(s + ", ");
        }
        System.out.println();
        for (Object s : setop.intersect(set1,set2)) {
            System.out.print(s + ", ");
        }
        System.out.println();
        for (Object s : setop.symetricSybstract(set1,set2)) {
            System.out.print(s + ", ");
        }
        System.out.println();
    }
}
