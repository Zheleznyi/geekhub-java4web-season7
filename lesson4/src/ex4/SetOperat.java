package ex4;

import java.util.Set;

public interface SetOperat {
    boolean equals(Set a, Set b);
    Set union(Set a, Set b);
    Set substract(Set a, Set b);
    Set intersect(Set a, Set b);
    Set symetricSybstract(Set a, Set b);
}
