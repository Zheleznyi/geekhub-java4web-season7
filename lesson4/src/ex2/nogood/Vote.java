package ex2.nogood;

public class Vote {
    private final String candidate;

    public Vote(String candidate){
        this.candidate = candidate;
    }

    @Override
    public String toString() {
        return candidate;
    }
}
