package ex2.nogood;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import ex2.nogood.Vote;

public class Test implements Collection {
    private Object[] data;
    private static final Object[] EMPTY_ELEMENTDATA = {};
    private static final Object[] DEFAULTCAPACITY_EMPTY_ELEMENTDATA = {};
    private int size;

    public Test(int initialCapacity) {
        if (initialCapacity > 0) {
            this.data = new Object[initialCapacity];
        } else if (initialCapacity == 0) {
            this.data = EMPTY_ELEMENTDATA;
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " +
                    initialCapacity);
        }
    }

    public Test() {
        this.data = DEFAULTCAPACITY_EMPTY_ELEMENTDATA;
    }


    @Override
    public int size() {
        return data.length;
    }

    @Override
    public boolean isEmpty() {
        return (size == 0);
    }

    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++)
                if (data[i] == null)
                    return i;
        } else {
            for (int i = 0; i < size; i++)
                if (o.equals(data[i]))
                    return i;
        }
        return -1;
    }

    @Override
    public boolean contains(Object o) {
        return (indexOf(o) >= 0);
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(data, size);
    }

    @Override
    public boolean add(Object o) {
        Vote vote1 = new Vote("Candidate№1");
        Vote vote2 = new Vote("Candidate№2");
        Vote vote3 = new Vote("Candidate№3");
        if (o.toString().equals(vote1.toString())) {
            return true;
        } else if (o.toString().equals(vote2.toString())) {
            size = size + 1;
            data = Arrays.copyOf(data, size);
            data[(size - 1)] = o;
            size = size + 1;
            data = Arrays.copyOf(data, size);
            data[(size - 1)] = o;
            return true;
        } else if (o.toString().equals(vote3.toString())) {
            size = size + 1;
            data = Arrays.copyOf(data, size);
            data[(size - 1)] = o;
            return true;
        } else {
            System.out.println("error");
            return false;
        }


    }

    public void whatIndex(Object o) {
        if (o.equals(2)) {

            int votes = (int) data[1];
            remove(1);
            votes = votes + 2;
            add(1, votes);
        } else if (o.equals(0)) {
            remove(0);
            int votes = 0;
            add(0, votes);
        } else if (o.equals(1)) {
            int votes = (int) data[2];
            remove(2);
            votes = votes + 1;
            add(2, votes);
        }
    }

    public void add(int index, Object o) {
        Vote vote1 = new Vote("Candidate№1");
        Vote vote2 = new Vote("Candidate№2");
        Vote vote3 = new Vote("Candidate№3");
        if (o.equals(vote1)) {

        } else if (o.equals(vote2)) {
            System.arraycopy(data, index, data, index + 1,
                    size - index);
            data[index] = o;
            size++;
            System.arraycopy(data, index + 1, data, index + 2,
                    size - index - 1);
            data[index + 1] = o;
            size++;
        } else if (o.equals(vote3)) {
            System.arraycopy(data, index, data, index + 1,
                    size - index);
            data[index] = o;
            size++;
        } else {
            System.out.println("error");
        }
    }

    public boolean remove(int index) {

        int numMoved = size - index - 1;
        if (numMoved > 0)
            System.arraycopy(data, index + 1, data, index,
                    numMoved);
        data[(size - 1)] = null;

        return true;
    }

    @Override
    public boolean remove(Object o) {

        if (o == null) {
            for (int index = 0; index < size; index++)
                if (data[index] == null) {
                    fastRemove(index);
                    size = size - 1;
                    return true;
                }
        } else {
            for (int index = 0; index < size; index++)
                if (o.equals(data[index])) {
                    fastRemove(index);
                    size = size - 1;
                    return true;
                }
        }
        return false;
    }

    private void fastRemove(int index) {
        int numMoved = size - index - 1;
        if (numMoved > 0)
            System.arraycopy(data, index + 1, data, index,
                    numMoved);
        data[--size] = null;
    }

    @Override
    public boolean addAll(Collection c) {
        Object[] a = c.toArray();
        int numNew = a.length;
        System.arraycopy(a, 0, data, size, numNew);
        size += numNew;
        return numNew != 0;
    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++)
            data[i] = null;

        size = 0;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        if (a.length < size)
            return (Object[]) Arrays.copyOf(data, size, a.getClass());
        System.arraycopy(data, 0, a, 0, size);
        if (a.length > size)
            a[size] = null;
        return a;
    }
}
