package ex2;

import ex2.nogood.Test;
import ex2.nogood.Vote;

public class Demo {
    public static void main(String[] args) {
        Test test = new Test();

        Vote vote1 = new Vote("Candidate№1");
        Vote vote2 = new Vote("Candidate№2");
        Vote vote3 = new Vote("Candidate№3");
        test.add(vote1);
        test.add(vote2);
        test.add(vote3);
        test.add(vote2);
        test.add(vote2);

        System.out.println(test.size());

    }
}
