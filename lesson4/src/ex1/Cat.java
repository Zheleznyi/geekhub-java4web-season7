package ex1;

import java.util.Objects;

public class Cat {
    String name;
    private final int[] rgbColor = new int[3];
    private final int age;

    public Cat(String name, int age, int colorOne, int colorTwo, int colorThree) {
        this.name = name;
        this.age = age;
        this.rgbColor[0] = colorOne;
        this.rgbColor[1] = colorTwo;
        this.rgbColor[2] = colorThree;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Cat cat = (Cat) obj;
        return Objects.equals(age, cat.age) && Objects.equals(rgbColor, cat.rgbColor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
