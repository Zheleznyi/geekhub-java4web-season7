package ex1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Demo {
    public static void main(String[] args) {
        HashMap newmap = new HashMap();
        newmap.put("Vasya", new Cat("Vasya", 22, 23, 22, 12));
        newmap.put("Jora", new Cat("Jora", 13, 44, 21, 123));
        newmap.put("Misha", new Cat("Misha", 5, 143, 121, 2));
        System.out.println("Check if key Vasya exists: " + newmap.containsKey("Vasya"));
        System.out.println("Check if key Jora exists: " + newmap.containsKey("Jora"));
        System.out.println("Check if key Misha exists: " + newmap.containsKey("Misha"));
        System.out.println("Check if value cat 'Vasya' exists: " + newmap.containsValue(new Cat("Vasya",
                22, 23, 22, 12)));
        System.out.println("Check if value cat 'Jora' exists: " + newmap.containsValue(new Cat("Jora",
                13, 44, 21, 123)));
        System.out.println("Check if value cat 'Misha' exists: " + newmap.containsValue(new Cat("Misha",
                5, 143, 121, 2)));
        HashSet newset = new HashSet();
        newset.add(new Cat("Vasya", 22, 23, 22, 12));
        newset.add(new Cat("Jora", 13, 44, 21, 123));
        newset.add(new Cat("Misha", 5, 143, 121, 2));
        System.out.println("Is the element 'Vasya' exists: " + newset.contains(new Cat("Vasya",
                22, 23, 22, 12)));
        System.out.println("Is the element 'Jora' exists: " + newset.contains(new Cat("Jora",
                13, 44, 21, 123)));
        System.out.println("Is the element 'Misha' exists: " + newset.contains(new Cat("Misha",
                5, 143, 121, 2)));

    }
}
