package ex5;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;

public class TestMap {
    public void hashMap() {
        long newtime = System.currentTimeMillis();
        HashMap hashMap = new HashMap();
        for (int i = 0; i < 11111; i++) {
            hashMap.put("1234", new Integer(1234));
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("Time for HashMap: add: " + newtime);
        for (int i = 0; i < 11111; i++) {
            hashMap.get(0);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("get: " + newtime);
        for (int i = 0; i < 11111; i++) {
            hashMap.replace("1234", 1234, 1);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("replace: " + newtime);
        for (int i = 0; i < 11111; i++) {
            hashMap.remove("1234");
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("remove: " + newtime);

    }

    public void linkedHashMap() {
        long newtime = System.currentTimeMillis();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (int i = 0; i < 11111; i++) {
            linkedHashMap.put("1234", new Integer(1234));
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("Time for LinkedHashMap: add: " + newtime);
        for (int i = 0; i < 11111; i++) {
            linkedHashMap.get(0);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("get: " + newtime);
        for (int i = 0; i < 11111; i++) {
            linkedHashMap.replace("1234", 1234, 1);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("replace: " + newtime);
        for (int i = 0; i < 11111; i++) {
            linkedHashMap.remove("1234");
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("remove: " + newtime);
    }

    public void treeMap() {
        long newtime = System.currentTimeMillis();
        TreeMap treeMap = new TreeMap();
        for (int i = 0; i < 11111; i++) {
            treeMap.put("1234", new Integer(1234));
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("Time for TreeMap: put: " + newtime);
        for (int i = 0; i < 11111; i++) {
            treeMap.get("1234");
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("get: " + newtime);
        for (int i = 0; i < 11111; i++) {
            treeMap.replace("1234", 1234, 1);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("replace: " + newtime);
        for (int i = 0; i < 11111; i++) {
            treeMap.remove("1234");
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("remove: " + newtime);

    }
}
