package ex5;

public class Test {
    public static void main(String[] args) {
        TestList testList = new TestList();
        testList.arrayList();
        testList.linkedList();
        testList.vector();

        TestSet testSet = new TestSet();
        testSet.hashSet();
        testSet.linkerHashSet();
        testSet.treeSet();

        TestMap testMap = new TestMap();
        testMap.hashMap();
        testMap.linkedHashMap();
        testMap.treeMap();
    }
}
