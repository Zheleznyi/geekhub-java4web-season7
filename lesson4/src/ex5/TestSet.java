package ex5;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class TestSet {
    public void hashSet() {
        long newtime = System.currentTimeMillis();
        HashSet hashSet = new HashSet();
        for (int i = 0; i < 11111; i++) {
            hashSet.add(new Integer(1232));
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("Time for Hashset: add: " + newtime);
        for (int i = 0; i < 11111; i++) {
            hashSet.contains(12);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("contains: " + newtime);
        for (int i = 0; i < 11111; i++) {
            hashSet.remove(788);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("remove: " + newtime);
    }

    public void linkerHashSet() {
        long newtime = System.currentTimeMillis();
        LinkedHashSet hashSet = new LinkedHashSet();
        for (int i = 0; i < 11111; i++) {
            hashSet.add(new Integer(1232));
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("Time for LinkedHashSet: add: " + newtime);
        for (int i = 0; i < 11111; i++) {
            hashSet.contains(12);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("contains: " + newtime);
        for (int i = 0; i < 11111; i++) {
            hashSet.remove(788);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("remove: " + newtime);
    }

    public void treeSet() {
        long newtime = System.currentTimeMillis();
        TreeSet hashSet = new TreeSet();
        for (int i = 0; i < 11111; i++) {
            hashSet.add(new Integer(1232));
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("Time for Treeset: add: " + newtime);
        for (int i = 0; i < 11111; i++) {
            hashSet.contains(12);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("contains: " + newtime);
        for (int i = 0; i < 11111; i++) {
            hashSet.remove(788);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("remove: " + newtime);
    }

}
