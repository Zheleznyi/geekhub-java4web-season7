package ex5;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Vector;

public class TestList {
    public void arrayList() {
        long newtime = System.currentTimeMillis();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < 11111; i++) {
            arrayList.add(new Integer(1232));
        }

        newtime = System.currentTimeMillis() - newtime;
        System.out.println("Time for arrayList: add: " + newtime);

        for (int i = 0; i < 11111; i++) {
            arrayList.get(1777);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("get: " + newtime);

        for (int i = 0; i < 11111; i++) {
            arrayList.set(11110, new Integer(829));
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("set: " + newtime);

        for (int i = 0; i < 11111; i++) {
            arrayList.remove(0);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("remove: " + newtime);


    }

    public void linkedList() {
        LinkedList linkedList = new LinkedList();
        long newtime = System.currentTimeMillis();
        for (int i = 0; i < 11111; i++) {
            linkedList.add(new Integer(1232));
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("Time for linkedList: add: " + newtime);
        for (int i = 0; i < 11111; i++) {
            linkedList.get(1777);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("get: " + newtime);
        for (int i = 0; i < 11111; i++) {
            linkedList.set(11110, new Integer(829));
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("set: " + newtime);
        for (int i = 0; i < 11111; i++) {
            linkedList.remove(0);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("remove: " + newtime);

    }

    public void vector() {
        Vector vector = new Vector();
        long newtime = System.currentTimeMillis();
        for (int i = 0; i < 11111; i++) {
            vector.add(new Integer(1232));
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("Time for Vector: add: " + newtime);
        for (int i = 0; i < 11111; i++) {
            vector.get(1777);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("get: " + newtime);
        for (int i = 0; i < 11111; i++) {
            vector.set(11110, new Integer(829));
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("set: " + newtime);
        for (int i = 0; i < 11111; i++) {
            vector.remove(0);
        }
        newtime = System.currentTimeMillis() - newtime;
        System.out.println("remove: " + newtime);
    }
}
