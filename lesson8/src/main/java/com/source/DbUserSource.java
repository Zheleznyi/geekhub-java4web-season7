package com.source;

import com.lisence.License;
import com.lisence.LicenseType;
import com.logger.Logger;
import com.user.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class DbUserSource implements UserSource {
    User currentUser;

    public DbUserSource(User currentUser) {
        this.currentUser = Objects.requireNonNull(currentUser);
        try (Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/moskalov",
                "postgres", "Fritz123"
        )
        ) {
            Statement statement = connection.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS tableOfUsers( Id SERIAL UNIQUE ," +
                    " Login VARCHAR(255) NOT NULL UNIQUE ," +
                    "Name VARCHAR(255) NOT NULL," +
                    "Admin BOOLEAN NOT NULL)";
            String sqlLicense = "CREATE TABLE IF NOT EXISTS tableOfLicense( UserId SERIAL UNIQUE ," +
                    "Type VARCHAR(255) NOT NULL UNIQUE ," +
                    "StartDate DATE NOT NULL ," +
                    "ExpirationDate DATE NOT NULL )";
            statement.executeUpdate(sql);
            statement.executeUpdate(sqlLicense);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Collection<User> getUsers() throws SQLException {
        String sql = "SELECT id,login,name,admin FROM tableofusers";
        ResultSet set = select(sql);
        String sqll = "SELECT userid,type,startdate,expirationdate FROM tableoflicense";
        ResultSet resultSet = select(sqll);
        Collection<User> users = new ArrayList<>();
        while (set.next()) {
            User user = new User(set.getString("login"), set.getString("name"),
                    set.getInt("id"));
            while (resultSet.next()){
                if (user.getId() == resultSet.getInt("userId")){
                    try {
                        user.addLicense(new License((LicenseType) resultSet.getObject("type"),
                                resultSet.getDate("startdate").toLocalDate(),
                                resultSet.getDate("expirationdate").toLocalDate()));
                    } catch (Exception e) {
                        Logger.out("sql exception");
                    }
                }
            }
            users.add(user);
        }
        return users;
    }

    @Override
    public void addUser(User user) throws SQLException {
        String sql = "SELECT id,login,name,admin FROM tableofusers";
        ResultSet set = select(sql);
        boolean res = true;
        while (set.next()) {
            if (set.getString("login").equals(user.getLogin())) {
                Logger.out("Change login of com.user: " + user.getFullName());
                res = false;
            } else if (set.getInt("id") == user.getId()) {
                Logger.out("Change id of com.user: " + user.getFullName());
                res = false;
            }
        }
        if (res) {
            addToDB(user);
        }

    }

    private void addToDB(User user) {
        try (Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/moskalov",
                "postgres", "Fritz123"
        )
        ) {
            Statement statement = connection.createStatement();
            String sql = "INSERT INTO tableofusers VALUES ('" +
                    user.getId() + "','" +
                    user.getLogin() + "','" +
                    user.getFullName() + "','" +
                    user.isAdmin() + "')";
            List<License> list = user.getLicenses();
            for (License lic : list) {
                String sqlL = "INSERT INTO tableoflicense VALUES ('" +
                        user.getId() + "','" +
                        lic.getType() + "','" +
                        lic.getStartDate() + "','" +
                        lic.getExpirationDate() + "')";
                statement.executeUpdate(sqlL);
            }

            statement.executeUpdate(sql);
        } catch (SQLException e) {
            Logger.out("sql exception");
        }
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    ResultSet select(String sql) {
        try (Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/moskalov",
                "postgres", "Fritz123"
        )
        ) {
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(sql);
            return set;
        } catch (SQLException e) {
            Logger.out("sql exception");
        }
        return null;
    }

}
