package com.source;

import com.user.User;

import java.sql.SQLException;
import java.util.Collection;

public interface UserSource {
    public Collection<User> getUsers() throws SQLException;

    void addUser(User user) throws SQLException;

    public User getCurrentUser();

    public void setCurrentUser(User currentUser);
}
