package com.service;



import com.lisence.License;
import com.source.UserSource;
import com.user.User;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class UserServiseImpl implements UserServise{
    private final UserSource userSource;
    private final UserValidator userValidator;


    public UserServiseImpl(UserSource userSource, UserValidator userValidator) {
        this.userSource = userSource;
        this.userValidator = userValidator;
    }

    @Override
    public void saveUser(User user) throws SQLException {
        userValidator.validateUser(user);
        userSource.addUser(user);
    }

    @Override
    public User findByLogin(String login) throws SQLException {
        List<User> users = findAll();
        for (int i = 0; i < findAll().size(); i++) {
            if (users.get(i).getLogin().equals(login)) {
                return users.get(i);
            }
        }
        throw new RuntimeException("We don't have this com.user");
    }

    @Override
    public List<User> findAll() throws SQLException {
        return new ArrayList<>(userSource.getUsers());
    }


    public List<User> findAllWhoExpiredAfterNDays(long n) throws SQLException {
        LocalDate date = LocalDate.now();
        date = date.plusDays(n);
        return findAllWhoExpiredInDay(date);
    }

    public List<User> findAllWhoExpiredInDay(LocalDate date) throws SQLException {
        List<User> list = new ArrayList<>(userSource.getUsers());
        List<User> users = new ArrayList<>();
        for (User user : list) {
            List<License> list1 = user.getLicenses();
            for (License aList1 : list1) {
                if (aList1.isActiveLicense()) {
                    if (aList1.getExpirationDate().equals(date)) {
                        users.add(user);
                    }
                }
            }
        }
        return users;
    }


}
