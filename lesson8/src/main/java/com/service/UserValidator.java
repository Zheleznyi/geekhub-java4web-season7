package com.service;

import com.source.UserSource;
import com.lisence.License;
import com.user.User;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public class UserValidator {
    private UserSource userSource;

    public UserValidator(UserSource userSource) {
        this.userSource = userSource;
    }

    public void validateUser(User user) throws SQLException {
        checkLoginAvailability(user);
        checkActiveLicense(user);
        checkLicensePresence(user);
    }

    private void checkLoginAvailability(User user) throws SQLException {
        Set<String> set = userSource.getUsers().stream().map(User::getLogin).collect(Collectors.toSet());
        boolean sets = set.contains(user.getLogin());
        if (sets) {
            throw new RuntimeException("Login is not create");
        }
    }

    private void checkLicensePresence(User user) {
        Collection<License> licenses = user.getLicenses();
        if (licenses.isEmpty()) {
            throw new RuntimeException("userAndLicense.com.user not have license");
        } else {
            boolean check = true;
            for (License license : licenses) {
                if (license.isActiveLicense()){
                    check = false;
                }
            }
            if (check){
                throw new RuntimeException();
            }
        }
    }

    private void checkActiveLicense(User user) {
        if (user.getLicenses().stream().noneMatch(License::isActiveLicense)) {
            throw new RuntimeException("Buy license please");
        }
    }
}
