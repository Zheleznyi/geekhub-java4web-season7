package com.service;

import com.user.User;

import java.sql.SQLException;
import java.util.List;

public interface UserServise {

    void saveUser(User user) throws SQLException;

    User findByLogin(String login) throws SQLException;

    List<User> findAll() throws SQLException;

}
