package com;

import com.lisence.License;
import com.lisence.LicenseType;
import com.logger.Logger;
import com.source.DbUserSource;
import com.user.User;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Collection;

public class Demo {
    public static void main(String[] args) {
        User user = new User("qq","qwerty",322);
        User userr = new User("qqq","qwertyq",3232);
        try {
            user.addLicense(new License(LicenseType.MOBILE_ONLY,
                    LocalDate.of(2017,12,2),
                    LocalDate.of(2018, 03, 1)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        DbUserSource db = new DbUserSource(userr);
        ManagementPanel mp = new ManagementPanel(db);
        try {
            //db.addUser(com.user);
            mp.removeUser(user);
            Collection<User> col = db.getUsers();
            for (User u: col) {
                Logger.out(u.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
