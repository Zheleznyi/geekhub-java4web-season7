package com.user;

import com.lisence.License;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class User implements Comparable<User>{
    private String login;
    private final String fullName;
    private boolean admin = false;
    private final List<License> licenses;
    private final int id;

    public User(String login, String fullName, int id){
        this.login = Objects.requireNonNull(login);
        this.fullName = Objects.requireNonNull(fullName);
        this.id = id;
        this.licenses = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFullName() {
        return fullName;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public List<License> getLicenses() {
        List<License> list = new ArrayList<>();
        list.addAll(licenses);
        return list;
    }

    public void addLicense(License license){
        this.licenses.add(license);
    }

    @Override
    public int compareTo(User o) {
        if (fullName.equals(o.fullName)){
            return login.compareTo(o.login);
        }
        return fullName.compareTo(o.fullName);
    }

    @Override
    public String toString() {
        return fullName;
    }
}
