package com.lisence;

import java.time.LocalDate;
import java.util.Objects;

public class License {
    private final LicenseType type;
    private final LocalDate startDate;
    private final LocalDate expirationDate;

    public License(LicenseType type, LocalDate startDate, LocalDate expirationDate) throws Exception {
        this.type = Objects.requireNonNull(type);
        this.startDate = Objects.requireNonNull(startDate);
        this.expirationDate = Objects.requireNonNull(expirationDate);
        if (startDate.isAfter(expirationDate)){
            throw new RuntimeException("Start date after expiration");
        }
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LicenseType getType() {
        return type;
    }

    public boolean isActiveLicense(){
        LocalDate today = LocalDate.now();
        return getExpirationDate().isAfter(today)&& !getStartDate().isAfter(today);
    }
}

