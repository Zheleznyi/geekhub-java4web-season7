package com;

import com.lisence.License;
import com.logger.Logger;
import com.service.UserServise;
import com.service.UserServiseImpl;
import com.service.UserValidator;
import com.source.UserSource;
import com.user.User;

import java.sql.*;
import java.time.LocalDate;
import java.util.*;

public class ManagementPanel {
    UserSource dBsource;

    public ManagementPanel(UserSource dBsource) {
        this.dBsource = dBsource;
    }

    public void createUser(User user) throws SQLException {
        if (!dBsource.getCurrentUser().isAdmin()) {
            throw new RuntimeException("You not admin");
        }
        dBsource.addUser(user);
    }

    public void updateUser(User user) {
        if (!dBsource.getCurrentUser().isAdmin()) {
            throw new RuntimeException("You not admin");
        }
        dBsource.setCurrentUser(user);
    }

    public void removeUser(User user) throws SQLException {
        Collection<User> users = dBsource.getUsers();
        if (dBsource.getCurrentUser().equals(user)) {
            throw new RuntimeException("You cant remove current com.user");
        }

        for (User user1 : users) {
            if (user.equals(user1)) {
                throw new RuntimeException("This com.user do not exist");
            }
        }
        try (Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/moskalov",
                "postgres", "Fritz123"
        )
        ) {
            Statement statement = connection.createStatement();
            String sql = "DELETE FROM tableofusers WHERE id = '" + user.getId() + "'";
            statement.executeUpdate(sql);
            sql = "DELETE FROM tableoflicense WHERE userid = '" + user.getId() + "'";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            Logger.out("Can not remove this com.user");
        }
    }

    public void printAll() throws SQLException {
        List<User> users = new ArrayList<>(dBsource.getUsers());
        Collections.sort(users);
        LocalDate date = LocalDate.now();
        boolean p = false;
        Logger.out("We have next users: ");
        for (User user : users) {
            Logger.out("Login - " + user.getLogin());
            Logger.out("Name - " + user.getFullName());
            Logger.out("userAndLicense.License info: ");
            for (License license : user.getLicenses()) {
                if (license.isActiveLicense()) {
                    Logger.out("Type: " + license.getType());
                    Logger.out("Expiration date:" + license.getExpirationDate());
                    Logger.out((license.getExpirationDate().minusDays(date.toEpochDay()) + "days left."));
                    p = true;
                }
            }
            if (!p)
                Logger.out("userAndLicense.com.user dont have active license.");
        }
    }

    public void printInNextWeek() throws SQLException {
        UserValidator userValidator = new UserValidator(dBsource);
        UserServise userServise = new UserServiseImpl(dBsource, userValidator);
        Collection<User> users = userServise.findAll();
        System.out.println("Users, who have expiration date license in next week: ");
        for (User user : users) {
            Logger.out("Login - " + user.getLogin());
            Logger.out("Name - " + user.getFullName());
            Logger.out("userAndLicense.License info: ");
            for (License license : user.getLicenses()) {
                if (license.isActiveLicense()) {
                    Logger.out("Type: " + license.getType());
                    Logger.out("Expiration date:" + license.getExpirationDate());
                    Logger.out((license.getExpirationDate()
                            .minusDays(LocalDate.now().toEpochDay()) + "days left."));
                }
            }
        }
    }

    public Map<Boolean, List<User>> usersIsAdminOrNot() throws SQLException {
        Map<Boolean, List<User>> map = new HashMap<>();
        List<User> users = new ArrayList<>(dBsource.getUsers());
        List<User> admins = new ArrayList<>();
        for (User user : users) {
            if (user.isAdmin()) {
                users.remove(user);
                admins.add(user);
            }
        }
        map.put(true, admins);
        map.put(false, users);
        return map;
    }


}
