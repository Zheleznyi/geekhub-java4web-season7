package ex3;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Read implements Runnable {
    public List<String> readFile(File file) throws IOException {
        FileReader fis = new FileReader(file);
        BufferedReader br = new BufferedReader(fis);
        List<String> list = new ArrayList<>();
        while (true) {
            String s = br.readLine();
            if (s == null) {
                break;
            }
            list.add(s);
        }
        br.close();
        return list;
    }

    public void outPair(List<String> list) throws IOException {
        for (int i = 0; i < list.size(); i = i + 2) {
            URL url = new URL(list.get(i));
            InputStream is = url.openConnection().getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            int valueBytes = 0;
            while (true) {
                int k = bis.read();
                if (k == -1) {
                    break;
                } else {
                    valueBytes++;
                }

            }
            System.out.println(list.get(i) + " - have size in bytes: " + valueBytes);
            bis.close();
        }
    }

    public void outUnPair(List<String> list) throws IOException {
        for (int i = 1; i < list.size(); i = i + 2) {
            URL url = new URL(list.get(i));
            InputStream is = url.openConnection().getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            int valueBytes = 0;
            while (true) {
                int k = bis.read();
                if (k == -1) {
                    break;
                } else {
                    valueBytes++;
                }

            }
            System.out.println(list.get(i) + " - have size in bytes: " + valueBytes);
            bis.close();
        }
    }

    @Override
    public void run() {
        try {
            outPair(readFile(new File("D:/Documents/idea/geekhub-java4web-season7/lesson12/src/ex3/links.txt")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
