package ex2;

import java.io.File;
import java.util.Scanner;

public class GetFiles {

    public String readPath() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input durectory pls:");
        return scanner.next();
    }

    public String readPattern() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input pattern pls:");
        return scanner.next();
    }

    public void getFiles(String path, String pattern) {
        File file = new File(path);
        File[] files = file.listFiles();
        System.out.println("This files have " + pattern + " pattern in directory:");
        for (File f : files) {
            String name = f.getName();
            if (name.contains(pattern)) {
                System.out.println(f);
            }
        }
    }
}
