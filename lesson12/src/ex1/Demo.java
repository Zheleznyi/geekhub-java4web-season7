package ex1;

import java.io.IOException;
import java.net.URL;

public class Demo {
    public static void main(String[] args) {
        Links links = new Links();
        new Thread(links).start();
        try {
            URL url = new URL("https://www.myscore.com.ua");
            links.forIndex(0, links.takeLinks(url), url);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
