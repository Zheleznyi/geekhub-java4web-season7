package ex1;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Links implements Runnable {
    public List<String> takeLinks(URL url) throws IOException {
        List<String> list = new ArrayList<>();
        String href = "<a href=";
        InputStreamReader reader = new InputStreamReader(url.openStream());
        BufferedReader br = new BufferedReader(reader);
        while (true) {
            String s = br.readLine();
            if (s == null) {
                break;
            }
            if (s.contains(href))
                list.add(s);
        }
        br.close();
        return list;
    }

    public void outCodeLinksPair(int index, List<String> list, URL url) throws IOException {
        String s = list.get(index);
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '<')
                if (chars[(i + 1)] == 'a')
                    if (chars[(i + 2)] == ' ')
                        if (chars[(i + 3)] == 'h')
                            if (chars[(i + 4)] == 'r') {
                                int a = i + 9;
                                s = "";
                                while (!(chars[a] == '"')) {
                                    s = s + chars[a];
                                    a++;
                                }
                            }
        }
        if (s.startsWith("/")) {
            s = "https://" + url.getHost() + s;
        }
        if (!s.equals("#")) {
            System.out.println(s);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(s).openConnection();
            System.out.println(httpURLConnection.getResponseCode());
        }

    }

    public void forIndex(int index, List<String> list, URL url) throws IOException {
        for (; index < list.size(); index = index + 2) {
            outCodeLinksPair(index, list, url);
        }
    }

    @Override
    public void run() {
        try {
            URL url = new URL("https://www.myscore.com.ua");
            forIndex(1, takeLinks(url), url);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
