package CO;

import java.util.Comparator;

public class Beer implements Comparator {
    private String whatIsBeer;

    Beer(String beer) {
        this.whatIsBeer = beer;
    }

    boolean isTasty() {
        boolean status = false;
        if (whatIsBeer.equals("Nice beer") || whatIsBeer.equals("Good beer")) {
            status = true;
        }
        return status;
    }

    @Override
    public String toString() {
        return whatIsBeer;
    }

    @Override
    public int compare(Object o1, Object o2) {
        return o1.toString().compareTo(o2.toString());
    }
}
