package CO;

import java.util.*;
import java.util.function.*;

public class COrelease implements CollectionOperattions {
    @Override
    public <E> List<E> fill(Supplier<E> producer, int count) {
        List<E> list = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            list.add(producer.get());
        }
        return list;
    }

    @Override
    public <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        List<E> list = new ArrayList<>();
        for (E element : elements) {
            if (filter.test(element)) {
                list.add(element);
            }
        }
        return list;
    }

    @Override
    public <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (predicate.test(element)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (!predicate.test(element)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (predicate.test(element)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public <T, R> List<R> map(List<T> elements, Function<T, R> mappingFunction) {
        List<R> list = new ArrayList<>();
        for (T element : elements) {
            list.add(mappingFunction.apply(element));
        }
        return list;
    }

    @Override
    public <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        E max = null;
        for(E element : elements){
            if(max.equals(null)){
                max = element;
            }
           int result = comparator.compare(element, max);
           if (result > 0){
               max = element;
           }
        }
        return Optional.ofNullable(max);
    }

    @Override
    public <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        E min = null;
        for(E element : elements){
            if(min.equals(null)){
                min = element;
            }
            int result = comparator.compare(element, min);
            if (result < 0){
                min = element;
            }
        }
        return Optional.ofNullable(min);
    }

    @Override
    public <E> List<E> distinct(List<E> elements) {
        for (E element : elements) {
            for (int i = 0; i < elements.size(); i++) {
                if (element.equals(elements.get(i))) {
                    elements.remove(i);
                }
            }
        }
        return elements;
    }

    @Override
    public <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for (E element: elements) {
            consumer.accept(element);
        }
    }

    @Override
    public <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        return reduce(elements, accumulator);
    }

    @Override
    public <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        return reduce(seed, elements, accumulator);
    }

    @Override
    public <E> Map<Boolean, List<E>> partitionBy(List<E> elements, Predicate<E> predicate) {
        List<E> list = new ArrayList<>();
        List<E> elements2 = new ArrayList<>(elements);
        Map<Boolean, List<E>> map = new HashMap<>();
        for (E element : elements) {
            if (predicate.test(element)) {
            } else {
                list.add(elements.get(elements.indexOf(element)));
                elements2.remove(element);
            }
        }
        map.put(true, elements2);
        map.put(false, list);
        return map;
    }

    @Override
    public <T, K> Map<K, List<T>> groupBy(List<T> elements, Function<T, K> classifier) {
        Map<K, List<T>> map = new HashMap<>();
        List<K> list = new ArrayList<>();
        for (int i = 0; i <= elements.size(); i++) {
            K result = classifier.apply(elements.get(i));
            for (K res : list) {
                if (res.equals(result)) {
                    List<T> list2 = new ArrayList<>();
                    list2.addAll(map.get(res));
                    list2.add(elements.get(i));
                    map.put(result, list2);
                } else {
                    List<T> list1 = new ArrayList<>();
                    list1.add(elements.get(i));
                    map.put(result, list1);
                }
            }
            list.add(result);
        }

        return map;
    }

    @Override
    public <T, K, U> Map<K, U> toMap(List<T> elements, Function<T, K> keyFunction, Function<T, U> valueFunction,
                                     BinaryOperator<U> mergeFunction) {
        Map<K, U> map = new HashMap<>();
        for (T element : elements) {
            K res = keyFunction.apply(element);
            U res1 = valueFunction.apply(element);
            if (map.containsKey(res)) {
                mergeFunction.apply(res1, res1);
            }
            map.put(res, res1);
        }
        return map;
    }
}
