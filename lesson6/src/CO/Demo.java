package CO;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Demo {
    public static void main(String[] args) {
        Predicate<Beer> pred = Beer::isTasty;
        Supplier<Beer> sup = () -> new Beer("Good beer");
        Supplier<Beer> sup1 = () -> new Beer("Nice beer");
        Supplier<Beer> sup2 = () -> new Beer("Bad beer");
        Function<Beer, Beer> function = (Beer) -> new Beer("Good beer");


        COrelease cOrelease = new COrelease();

        List<Beer> list = cOrelease.fill(sup2, 74);
        List<Beer> list1 = cOrelease.fill(sup, 32);
        list1.addAll(cOrelease.fill(sup1, 21));
        list1.addAll(cOrelease.fill(sup2, 11));
        System.out.println(list1.size());
        for (Beer beer : list1) {
            System.out.println(beer);
        }
        List<Beer> list2 = cOrelease.filter(list1, pred);
        System.out.println(list.size());
        System.out.println(cOrelease.anyMatch(list, pred));
        System.out.println(cOrelease.allMatch(list, pred));
        System.out.println(cOrelease.noneMatch(list, pred));
        cOrelease.map(list, function);


    }
}

