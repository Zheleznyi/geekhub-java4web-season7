<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Form</title>
</head>
<body>
<form action=/main/form method=get>
    Action:<br>
    <select name=action>
        <option value=></option>
        <option value="add">Add</option>
        <option value="update">Update</option>
        <option value=invalidate>Invalidate</option>
        <option value=delete>Remove</option>
    </select>
    </br>
    <span style=color:black;>Name:</span></br>
    <input type=text name=name>
    <br>
    Value:<br>
    <input type=text name=value>
    <br><br>
    <input type=submit value=Submit>
</form>
<h1>${name}</h1>
<h2>${value}</h2>
</body>
</html>
