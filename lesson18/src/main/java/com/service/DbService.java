package com.service;

import java.sql.*;

public class DbService {

    public DbService(){

    }

    public void dBUpdate(String sql){
        try (Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/moskalov",
                "postgres", "Fritz123"
        )
        ){
            Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public ResultSet select(String sql){
        try(Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/moskalov",
                "postgres", "Fritz123"
        )){
            Statement statement = connection.createStatement();
            return statement.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
