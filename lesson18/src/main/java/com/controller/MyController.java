package com.controller;

import com.dto.User;
import com.service.DbService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/main")
public class MyController {
    private DbService dbService;

    @GetMapping(value = "form")
    public String byStringViewName(@RequestParam String action,@RequestParam String name, @RequestParam String value,
                                         ModelMap model) throws SQLException {
        dbService = new DbService();
        dbService.dBUpdate("CREATE TABLE IF NOT EXISTS tableUsers(" +
                "Name VARCHAR(255) NOT NULL,"+
                "Value VARCHAR(255) NOT NULL)");
        User user = new User(name, value);
        dbService.dBUpdate("INSERT INTO tableUsers VALUES('" +
                user.getName()+"','" +
                user.getValue()+"')");
        ResultSet rs = dbService.select("SELECT name,value FROM tableUsers");
        Map<String,String> map = new HashMap<>();
        while (rs != null && rs.next()){
            map.put(rs.getString("name"), rs.getString("value"));
        }
        model.addAttribute("name", map.keySet());
        model.addAttribute("value", map.values());
        return "form";
    }

}
