package com.geekhub.lessons.services;

import com.geekhub.lessons.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class DbService {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DbService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS users_lesson21(" +
                "name VARCHAR(255) NOT NULL," +
                "value VARCHAR(255) NOT NULL )");
    }

    public void add(String name, String value) {
        jdbcTemplate.update("INSERT INTO users_lesson21 VALUES (?,?)", name, value);
    }

    public void update(String name, String value) {
        jdbcTemplate.update("UPDATE users_lesson21 SET value = ? WHERE name = ?", value, name);
    }

    public void invalidate() {
        jdbcTemplate.update("DELETE FROM users_lesson21");
    }

    public void delete(String name, String value) {
        jdbcTemplate.update("DELETE  FROM users_lesson21 WHERE name = ? AND value = ?", name, value);
    }

    public List<User> getAllUsers() {
        return jdbcTemplate.query("SELECT name,value FROM users_lesson21",
                (rs, rowNum) -> new User(rs.getString("name"), rs.getString("value")));
    }

    public List<User> getAllUsers2() {
        List<User> users = new ArrayList<>();
        List<String> names = jdbcTemplate.queryForList("SELECT name FROM users_lesson21", String.class);
        List<String> values = jdbcTemplate.queryForList("SELECT value FROM users_lesson21", String.class);
        for (int i = 0; i < names.size(); i++) {
            users.add(new User(names.get(i), values.get(i)));
        }
        return users;
    }

    public User getUserWhenEquals(String name,String value) {
        String valueNew = jdbcTemplate.queryForObject("SELECT value FROM users_lesson21 WHERE value=?",
                new Object[]{value}, String.class);
        String nameNew = jdbcTemplate.queryForObject("SELECT name FROM users_lesson21 WHERE name=?",
                new Object[]{name}, String.class);
        return new User(nameNew,valueNew);
    }

}
