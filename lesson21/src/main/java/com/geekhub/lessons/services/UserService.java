package com.geekhub.lessons.services;

import com.geekhub.lessons.dto.User;

import java.util.ArrayList;
import java.util.List;


public class UserService {
    private final DbService dbService;
    private final User user;

    public UserService(String action, String user, String value, DbService dbService) {
        validate(user);
        validate(value);
        this.user = new User(user, value);
        this.dbService = dbService;
        choice(action);
    }

    private void choice(String action) {
        switch (action) {
            case "add":
                dbService.add(user.getName(), user.getValue());
                break;
            case "delete":
                dbService.delete(user.getName(), user.getValue());
                break;
            case "invalidate":
                dbService.invalidate();
                break;
            case "update":
                dbService.update(user.getName(), user.getValue());
                break;
            default:
                throw new IllegalArgumentException("pls choose case");
        }
    }

    private void validate(String param) {
        if (param == null) {
            param = "";
        }
        if (param.isEmpty()){
            throw new IllegalArgumentException("pls input data");
        }
    }

    public List<User> getAllUsers(){
        return dbService.getAllUsers2();
    }


}




