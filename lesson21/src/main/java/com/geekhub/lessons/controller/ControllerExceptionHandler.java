package com.geekhub.lessons.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice(basePackages = {"com.geekhub.lessons"}, assignableTypes = {MainController.class})
public class ControllerExceptionHandler {
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(IllegalArgumentException.class)
    public ModelAndView exceptionHandler(Exception ex){
        ModelAndView mav = new ModelAndView("main");
        mav.addObject("message", ex.getMessage());
        return mav;
    }
}
