package com.geekhub.lessons.controller;

import com.geekhub.lessons.services.DbService;
import com.geekhub.lessons.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {
    @Autowired
    private DbService dbService;

    @RequestMapping
    public String main(@RequestParam(required = false) String action, @RequestParam(required = false) String name,
                       @RequestParam(required = false) String value, Model model) {
        UserService userService = new UserService(action, name, value, dbService);
        model.addAttribute("users", userService.getAllUsers());
        model.addAttribute("message", "");
        return "main";
    }
}
