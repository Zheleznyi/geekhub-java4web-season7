import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Db {

    public static void main(String[] args) {
        try (Connection connection = DriverManager.getConnection(
                     "jdbc:h2:~/public",
                     "moskalov", "123123"
             )
        ) {
            Statement statement = connection.createStatement();
            String sql = "CREATE TABLE users( Number INT NOT NULL ," +
                    " Name VARCHAR(255) NOT NULL ," +
                    "Age INT NOT NULL )";
            statement.executeUpdate(sql);
            String sqsl = "INSERT INTO users VALUES (2,'fox',12)";
            statement.executeUpdate(sqsl);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
