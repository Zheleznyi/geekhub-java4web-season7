package com.geekhub.project.dto;

import java.util.ArrayList;
import java.util.List;

public class TimeTable {
    private List<String> list = new ArrayList<>();
    private String day;

    public void setDay(String day) {
        this.day = day;
    }

    public List<String> getList() {
        return list;
    }

    public String getDay() {
        return day;
    }

    public void addList(String add){
        list.add(add);
    }
}

