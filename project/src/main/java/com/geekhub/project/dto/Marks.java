package com.geekhub.project.dto;

import java.util.ArrayList;
import java.util.List;

public class Marks {
    private List<Integer> marks = new ArrayList<>();
    private String surname;

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void addMark(Integer mark){
        marks.add(mark);
    }

    public List<Integer> getMarks() {
        return marks;
    }
}
