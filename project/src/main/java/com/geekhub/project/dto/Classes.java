package com.geekhub.project.dto;

import java.util.ArrayList;
import java.util.List;

public class Classes {
    private List<String> classes = new ArrayList<>();

    public List<String> getClasses() {
        return classes;
    }

    public void isMyClass(boolean bol, String name) {
        if (bol) {
            addClass(whatClass(name) + "-A");
            addClass(whatClass(name) + "-B");
            addClass(whatClass(name) + "-C");
        }
    }

    private String whatClass(String name) {
        switch (name) {
            case "one":
                return 1 + "";
            case "two":
                return 2 + "";
            case "three":
                return 3 + "";
            case "four":
                return 4 + "";
            case "five":
                return 5 + "";
            case "six":
                return 6 + "";
            case "seven":
                return 7 + "";
            case "eight":
                return 8 + "";
            case "nine":
                return 9 + "";
            case "ten":
                return 10 + "";
            case "eleven":
                return 11 + "";
        }
        return null;
    }

    public void addClass(String schclass) {
        classes.add(schclass);
    }
}
