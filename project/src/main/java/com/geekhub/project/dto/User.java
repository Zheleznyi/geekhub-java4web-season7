package com.geekhub.project.dto;

public class User {
    private int id;
    private final String login;
    private final String password;
    private String classorsubject;
    private String name;
    private String surname;
    private String role;

    public User(int id, String name, String surname, String password, String role, String login, String classorsubject) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.role = role;
        this.id = id;
        this.classorsubject = classorsubject;
    }

    public User(String name, String surname, String password, String role, String login, String classorsubject) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.role = role;
        id = 0;
        this.classorsubject = classorsubject;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClassorsubject() {
        return classorsubject;
    }

    public void setClassorsubject(String classorsubject) {
        this.classorsubject = classorsubject;
    }
}
