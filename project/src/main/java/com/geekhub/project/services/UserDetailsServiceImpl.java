package com.geekhub.project.services;

import com.geekhub.project.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private DbService dbService;

    @Autowired
    public UserDetailsServiceImpl(DbService dbService){
        this.dbService = dbService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = dbService.getUserByLogin(username);
        if (user!=null) {
            return new org.springframework.security.core.userdetails.User(
                    user.getLogin(), user.getPassword(), Collections.singletonList(new SimpleGrantedAuthority(user.getRole()))
            );
        } else{
            throw new UsernameNotFoundException("Incorrect data or password");
        }
    }
}
