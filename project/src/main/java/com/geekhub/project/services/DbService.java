package com.geekhub.project.services;

import com.geekhub.project.dto.Classes;
import com.geekhub.project.dto.Marks;
import com.geekhub.project.dto.TimeTable;
import com.geekhub.project.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DbService {
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final ShaPasswordEncoder passwordEncoder;
    private List<String> numbers = new ArrayList<>();


    @Autowired
    public DbService(NamedParameterJdbcTemplate jdbcTemplate, ShaPasswordEncoder passwordEncoder) {
        this.jdbcTemplate = jdbcTemplate;
        this.passwordEncoder = passwordEncoder;
        numbers.add("one");
        numbers.add("two");
        numbers.add("three");
        numbers.add("four");
        numbers.add("five");
        numbers.add("six");
        numbers.add("seven");
        numbers.add("eight");
        numbers.add("nine");
        numbers.add("ten");
        numbers.add("eleven");
        numbers.add("twelve");
        numbers.add("thirteen");
        numbers.add("fourteen");
        numbers.add("fifteen");
        numbers.add("sixteen");
        numbers.add("seventeen");
        numbers.add("eightteen");
        numbers.add("nineteen");
        numbers.add("twenty");
    }

    public List<String> getNumbers() {
        return numbers;
    }

    public void addSubjectInClass(String schclass, String subject){
        Map<String,Object> param = new HashMap<>();
        for (User user : getUsersInClass(schclass)){
            param.put("id",user.getId());
            param.put("name",user.getName());
            param.put("sur",user.getSurname());
            param.put("classorsubject",user.getClassorsubject());
            param.put("subject",subject);
            jdbcTemplate.update("INSERT INTO subjects (id,name, surname, schclass,subject) VALUES(:id,:name,:sur,:classorsubject,:subject)",param);
        }
    }

    public void addUser(User user) {
        if (user.getId() == 0) {
            user.setId(getNewId());
        }
        if (isUniqueId(user.getId())) {
            Map<String, Object> map = new HashMap<>();
            map.put("id", user.getId());
            map.put("name", user.getName());
            map.put("sur", user.getSurname());
            map.put("password", passwordEncoder.encodePassword(user.getPassword(), null));
            map.put("role", user.getRole());
            map.put("login", user.getLogin());
            map.put("classorsubject", user.getClassorsubject());
            jdbcTemplate.update("INSERT INTO users VALUES(:id,:name,:sur,:password,:role,:login,:classorsubject)"
                    , map);
            if (user.getRole().equals("ROLE_USER")) {
                List<String> subjects = getSubjects(user.getClassorsubject());
                for (String subject : subjects) {
                    map.put("subject", subject);
                    System.out.println(subject+user.getId()+user.getName()+user.getRole()+user.getClassorsubject()+user.getSurname()+"\n\n\n\n\n\n");
                    jdbcTemplate.update("INSERT INTO subjects (id,name, surname, schclass,subject) VALUES(:id,:name,:sur,:classorsubject,:subject)", map);
                }
            } else if (user.getRole().equals("ROLE_TEACHER")) {
                map.put("subject",user.getClassorsubject());
                addTeacherInTable(map);
            }
        } else {
            throw new RuntimeException("Id has");
        }
    }

    public List<User> getAllUsers() {
        return jdbcTemplate.query("SELECT id,name,surname,password,role,login,classorsubject FROM users", (rs, rowNum) ->
                new User(rs.getInt("id"), rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("password"), rs.getString("role"),
                        rs.getString("login"), rs.getString("classorsubject")));
    }

    public User getUserByLogin(String login) {
        for (User user : getAllUsers()) {
            if (user.getLogin().equals(login)) {
                return user;
            }
        }
        throw new RuntimeException("This user do not exist");
    }

    public List<User> getUsersInClass(String schClass) {
        return jdbcTemplate.query("SELECT id,name,surname,password,role,login,classorsubject FROM users " +
                        "WHERE classorsubject = '" + schClass + "'",
                (rs, rowNum) ->
                        new User(rs.getInt("id"), rs.getString("name"),
                                rs.getString("surname"),
                                rs.getString("password"), rs.getString("role"),
                                rs.getString("login"), rs.getString("classorsubject")));

    }

    public boolean isUniqueId(int id) {
        for (User user : getAllUsers()) {
            if (user.getId() == (id)) {
                return false;
            }
        }
        return true;
    }

    private int getNewId() {
        int id = 0;
        for (User user : getAllUsers()) {
            if (user.getId() > (id)) {
                id = user.getId();
            }
        }
        id = id + 1;
        return id;
    }

    public List<String> getSubjects(String schclass) {
        Map<String,Object> param = new HashMap<>();
        param.put("schclass",schclass);
        param.put("surname",getUsersInClass(schclass).get(0).getSurname());
        return jdbcTemplate.queryForList("SELECT subject FROM subjects WHERE schclass = :schclass AND surname = :surname",
                param,String.class);
    }

    public String chooseSubject() {
        return "SELECT surname,one,two,three,four,five,six,seven," +
                "eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,eightteen,nineteen," +
                "twenty,exam FROM subjects WHERE subject = :subject AND surname = :surname";
    }

    private List<Marks> queryForSubject(Map<String, Object> param) {
        return jdbcTemplate.query(chooseSubject(), param, (rs, rowNum) -> {
            Marks marks1 = new Marks();
            marks1.setSurname(rs.getString("surname"));
            marks1.addMark(rs.getInt("one"));
            marks1.addMark(rs.getInt("two"));
            marks1.addMark(rs.getInt("three"));
            marks1.addMark(rs.getInt("four"));
            marks1.addMark(rs.getInt("five"));
            marks1.addMark(rs.getInt("six"));
            marks1.addMark(rs.getInt("seven"));
            marks1.addMark(rs.getInt("eight"));
            marks1.addMark(rs.getInt("nine"));
            marks1.addMark(rs.getInt("ten"));
            marks1.addMark(rs.getInt("eleven"));
            marks1.addMark(rs.getInt("twelve"));
            marks1.addMark(rs.getInt("thirteen"));
            marks1.addMark(rs.getInt("fourteen"));
            marks1.addMark(rs.getInt("fifteen"));
            marks1.addMark(rs.getInt("sixteen"));
            marks1.addMark(rs.getInt("seventeen"));
            marks1.addMark(rs.getInt("eightteen"));
            marks1.addMark(rs.getInt("nineteen"));
            marks1.addMark(rs.getInt("twenty"));
            marks1.addMark(rs.getInt("exam"));
            return marks1;
        });
    }

    public Map<String, List<Integer>> getMarksOfUser(String schClass, String subject, int index) {
        Map<String, List<Integer>> map = new HashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("subject", subject);
        for (User user : getUsersInClass(schClass)) {
            param.put("surname", user.getSurname());
            param.put("subject", subject);
            List<Marks> marks = queryForSubject(param);
            for (Marks mark : marks) {
                List<Integer> newmarks = new ArrayList<>();
                for (int i = 0; i < index; i++) {
                    newmarks.add(mark.getMarks().get(i));
                }
                map.put(mark.getSurname(), newmarks);
            }
        }
        return map;
    }

    public Map<String, List<String>> getMarksWithSemester(Map<String, List<Integer>> map) {
        Map<String, List<String>> newMap = new HashMap<>();
        for (String key : map.keySet()) {
            newMap.put(key, addSemestermark(map.get(key)));
        }
        return newMap;
    }

    private List<String> addSemestermark(List<Integer> marks) {
        List<String> newMarks = new ArrayList<>();
        int count = 0;
        int semester = 0;
        for (Integer i : marks) {
            if (i != 0) {
                count = count + 1;
                semester = semester + i;
                newMarks.add(i + "");
            } else {
                newMarks.add("");
            }
        }
        if (count != 0) {
            semester = semester / count;
        }
        if (semester != 0) {
            newMarks.add(semester + "");
        } else {
            newMarks.add("");
        }
        return newMarks;
    }

    private String chooseSubjectDates() {
        return "SELECT one,two,three,four,five,six,seven," +
                "eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,eightteen,nineteen," +
                "twenty FROM tabledates WHERE subject = :subject AND schclass = :schclass";
    }

    public List<String> getSevenMarks(Map<String, List<Integer>> map, String surname) {
        List<String> marks = new ArrayList<>();
        List<String> marks1 = new ArrayList<>();
        List<Integer> list = map.get(surname);
        marks.add(list.get(4) + "");
        marks.add(list.get(8) + "");
        marks.add(list.get(9) + "");
        marks.add(list.get(13) + "");
        marks.add(list.get(17) + "");
        marks.add(list.get(18) + "");
        marks.add(list.get(19) + "");
        marks.add(list.get(20) + "");
        for (String mark : marks) {
            if (mark.equals("0")) {
                marks1.add("");
            } else {
                marks1.add(mark);
            }
        }
        return marks1;
    }

    public List<String> getDatesClass(String schclass, String subject) {
        Map<String, String> param = new HashMap<>();
        param.put("schclass", schclass);
        param.put("subject", subject);
        return jdbcTemplate.queryForObject(chooseSubjectDates(), param, (rs, rowNum) -> {
            List<String> list = new ArrayList<>();
            list.add(rs.getString("one"));
            list.add(rs.getString("two"));
            list.add(rs.getString("three"));
            list.add(rs.getString("four"));
            list.add(rs.getString("five"));
            list.add(rs.getString("six"));
            list.add(rs.getString("seven"));
            list.add(rs.getString("eight"));
            list.add(rs.getString("nine"));
            list.add(rs.getString("ten"));
            list.add(rs.getString("eleven"));
            list.add(rs.getString("twelve"));
            list.add(rs.getString("thirteen"));
            list.add(rs.getString("fourteen"));
            list.add(rs.getString("fifteen"));
            list.add(rs.getString("sixteen"));
            list.add(rs.getString("seventeen"));
            list.add(rs.getString("eightteen"));
            list.add(rs.getString("nineteen"));
            list.add(rs.getString("twenty"));
            return list;
        });
    }

    public List<User> getAllTeachers(){
        List<User> users = new ArrayList<>();
        for (User user:getAllUsers()){
            if (user.getRole().equals("ROLE_TEACHER")){
                users.add(user);
            }
        }
        return users;
    }



    public List<List<String>> getTimeTable(String schclass) {
        Map<String, String> param = new HashMap<>();
        param.put("schclass", schclass);
        List<TimeTable> list = getTimeTableList(param);
        List<List<String>> ret = new ArrayList<>();
        for (TimeTable timeTable : list) {
            ret.add(timeTable.getList());
        }
        return ret;
    }

    private List<TimeTable> getTimeTableList(Map<String, String> param) {
        return jdbcTemplate.query("SELECT day,one,two,three,four,five,six,seven,eight FROM timeclass " +
                        "WHERE schclass = :schclass",
                param, (rs, rowNum) -> {
                    TimeTable timeTable = new TimeTable();
                    timeTable.addList(rs.getString("day"));
                    timeTable.addList(rs.getString("one"));
                    timeTable.addList(rs.getString("two"));
                    timeTable.addList(rs.getString("three"));
                    timeTable.addList(rs.getString("four"));
                    timeTable.addList(rs.getString("five"));
                    timeTable.addList(rs.getString("six"));
                    timeTable.addList(rs.getString("seven"));
                    timeTable.addList(rs.getString("eight"));
                    return timeTable;
                });
    }

    private void addTeacherInTable(Map map) {
        jdbcTemplate.update("INSERT INTO teachclasses(id,surname,login,subject) VALUES(:id,:sur,:login,:subject)"
                , map);
    }

    public List<String> getClasessTeachers(User user) {
        Map<String, Object> param = new HashMap<>();
        param.put("login", user.getLogin());
        Classes classes = jdbcTemplate.queryForObject("SELECT one,two,three,four,five,six,seven,eight,nine,ten,eleven FROM teachclasses " +
                "WHERE login = :login", param, (rs, rowNum) -> {
            Classes classes1 = new Classes();
            classes1.isMyClass(rs.getBoolean("one"), "one");
            classes1.isMyClass(rs.getBoolean("two"), "two");
            classes1.isMyClass(rs.getBoolean("three"), "three");
            classes1.isMyClass(rs.getBoolean("four"), "four");
            classes1.isMyClass(rs.getBoolean("five"), "five");
            classes1.isMyClass(rs.getBoolean("six"), "six");
            classes1.isMyClass(rs.getBoolean("seven"), "seven");
            classes1.isMyClass(rs.getBoolean("eight"), "eight");
            classes1.isMyClass(rs.getBoolean("nine"), "nine");
            classes1.isMyClass(rs.getBoolean("ten"), "ten");
            classes1.isMyClass(rs.getBoolean("eleven"), "eleven");
            return classes1;
        });
        return classes.getClasses();
    }

    public void addClassTeacher(String schclass, String login){
        Map<String,Object> param = new HashMap<>();
        param.put("login",login);
        jdbcTemplate.update("UPDATE teachclasses SET two = TRUE WHERE login = :login",param);
    }


    public void whatDo(Map<String, String> params, String subject, String schclass, int size) {
        if (params.get("date") != null && params.get("date").equals("input")) {
            changeDate(params.get("inputdate"), subject, getNumbers().get(size), schclass);
        } else if (params.get("date") != null && !params.get("inputdate").equals("")) {
            for (String number : getNumbers()) {
                if (params.get("date").equals(number)) {
                    changeDate(params.get("inputdate"), subject, number, schclass);
                }
            }
        }
        if (params.get("value") != null && params.get("inputdate").equals("")&&!params.get("value").equals("")) {
            User user = getUserByLogin(params.get("user"));
            changeMark(params.get("value"), subject, params.get("date"), schclass, user.getSurname());
        }
        if (params.get("namehw") != null && !params.get("namehw").equals("")) {
            changeHomework(schclass, subject, params.get("namehw"), params.get("hw"));
        }
        if (params.get("examvalue")!=null&&!params.get("examvalue").equals("")){
            User user = getUserByLogin(params.get("user"));
            changeMark(params.get("examvalue"), subject,"exam", schclass, user.getSurname());
        }
    }

    public void changeDate(String date, String subject, String number, String schclass) {
        Map<String, Object> param = new HashMap<>();
        String sql = chooseUpdateDateSubject(number);
        param.put("date", date);
        param.put("schclass", schclass);
        param.put("subject",subject);
        jdbcTemplate.update(sql, param);
    }

    public void changeMark(String date, String subject, String number, String schclass, String surname) {
        Map<String, Object> param = new HashMap<>();
        String sql = chooseUpdateMark(number);
        param.put("date", whatMark(date));
        param.put("schclass", schclass);
        param.put("surname", surname);
        param.put("subject",subject);
        jdbcTemplate.update(sql, param);
    }

    private int whatMark(String date) {
        switch (date) {
            case "1":
                return 1;
            case "2":
                return 2;
            case "3":
                return 3;
            case "4":
                return 4;
            case "5":
                return 5;
            case "6":
                return 6;
            case "7":
                return 7;
            case "8":
                return 8;
            case "9":
                return 9;
            case "10":
                return 10;
            case "11":
                return 11;
            case "12":
                return 12;
        }
        return 0;
    }

    public String chooseUpdateMark(String number) {
        return "UPDATE subjects SET " + number + " = :date WHERE subject = :subject AND schclass = :schclass AND surname = :surname";
    }


    public String chooseUpdateDateSubject(String number) {
        return "UPDATE tabledates SET " + number + " = :date WHERE subject = :subject AND schclass = :schclass";
    }

    public List<String> getExamDates(List<String> schclasses, String subject) {
        Map<String, Object> param = new HashMap<>();
        List<String> list = new ArrayList<>();
        for (String schclass : schclasses) {
            param.put("schclass", schclass);
            param.put("subject",subject);
            param.put("surname", getUsersInClass(schclass).get(0).getSurname());
            String get = jdbcTemplate.queryForObject("SELECT dateexam FROM subjects WHERE " +
                            "subject = :subject AND schclass = :schclass" +
                            " AND surname = :surname",
                    param, String.class);
            list.add(get);
        }
        return list;
    }

    public void updateDateExam(Map<String, String> map, String subject) {
        for (String schclass : map.keySet()) {
            if (map.get(schclass) != null && !map.get(schclass).equals("")) {
                Map<String, String> param = new HashMap<>();
                param.put("schclass", schclass);
                param.put("subject",subject);
                param.put("date", map.get(schclass));
                jdbcTemplate.update("UPDATE subjects SET dateexam = :date WHERE subject = :subject AND " +
                        "schclass = :schclass", param);
            }
        }
    }

    public void updateUser(Map<String, String> map, String login) {
        if (map.get("action").equals("change")){
            if (map.get("parameter") != null && !map.get("parameter").equals("")) {
                Map<String, Object> param = new HashMap<>();
                param.put("key", map.get("parametr"));
                param.put("value", map.get("parameter"));
                param.put("login", login);
                jdbcTemplate.update("UPDATE users SET " + map.get("parametr") + " = :value WHERE login = :login", param);
            }
        } else {
            Map<String, Object> param = new HashMap<>();
            param.put("login", login);
            jdbcTemplate.update("DELETE FROM users WHERE login = :login", param);
        }

    }

    public List<String> getDatesExamForUser(User user) {
        List<String> subjects = getSubjects(user.getClassorsubject());
        Map<String, Object> param = new HashMap<>();
        List<String> dates = new ArrayList<>();
        param.put("id", user.getId());
        for (String subject : subjects) {
            param.put("subject",subject);
            String date = jdbcTemplate.queryForObject("SELECT dateexam FROM subjects WHERE subject = :subject" +
                    " AND id = :id", param, String.class);
            dates.add(date);
        }
        return dates;
    }

    public List<String> getActualDates(List<String> dates) {
        List<String> newDates = new ArrayList<>();
        for (String date : dates) {
            if (!date.equals("-")) {
                newDates.add(date);
            }
        }
        return newDates;
    }

    public List<String> getHomeworksOfClass(String schclass, String subject) {
        Map<String, String> param = new HashMap<>();
        param.put("schclass", schclass);
        param.put("subject", subject);
        return jdbcTemplate.queryForList("SELECT hw FROM homeworks WHERE schclass = :schclass AND subject = :subject",
                param, String.class);
    }

    public List<String> getNamesHomeworksOfClass(String schclass, String subject) {
        Map<String, String> param = new HashMap<>();
        param.put("schclass", schclass);
        param.put("subject", subject);
        return jdbcTemplate.queryForList("SELECT namehw FROM homeworks WHERE schclass = :schclass AND subject = :subject",
                param, String.class);
    }

    private void changeHomework(String schclass, String subject, String namehw, String hw) {
        Map<String, String> param = new HashMap<>();
        param.put("schclass", schclass);
        param.put("subject", subject);
        param.put("namehw", namehw);
        param.put("hw", hw);
        if (getNamesHomeworksOfClass(schclass, subject).contains(namehw)) {
            jdbcTemplate.update("UPDATE homeworks SET hw = :hw WHERE " +
                    "namehw = :namehw AND schclass = :schclass AND subject = :subject", param);
        } else {
            jdbcTemplate.update("INSERT INTO homeworks(namehw, schclass, subject, hw) VALUES " +
                    "(:namehw,:schclass,:subject,:hw)", param);
        }
    }

    public void changeTimeTable(String classorlogin, String day, String numberLesson) {
        Map<String, String> param = new HashMap<>();
        param.put("schclass", classorlogin);
        param.put("day", day);
        jdbcTemplate.update("UPDATE timeclass SET" + numberLesson + " WHERE schclass = :schclass AND" +
                " day = :day", param);
    }

    public List<String> getAllClasess() {
        Map<String, String> param = new HashMap<>();
        List<String> list = jdbcTemplate.queryForList("SELECT schclass FROM timeclass", param, String.class);
        return list.stream().distinct().collect(Collectors.toList());
    }

    public Map<String, List<String>> sortClass(List<String> allclasses) {
        Map<String, List<String>> map = new HashMap<>();
        List<String> classes = new ArrayList<>();
        List<String> teachers = new ArrayList<>();
        for (String str : allclasses) {
            if (str.length() < 5) {
                classes.add(str);
            } else {
                teachers.add(str);
            }
        }
        map.put("teachers", teachers);
        map.put("classes", classes);
        return map;
    }

    public List<String> getAllSubjectsForClass(String schclass) {
        Map<String, Integer> param = new HashMap<>();
        User user;
        if (schclass.length() < 5) {
            user = getUsersInClass(schclass).get(0);
        } else {
            user = getUserByLogin(schclass);
        }
        param.put("id", user.getId());
        return jdbcTemplate.queryForList("SELECT subject FROM subjects WHERE id = :id", param, String.class);
    }

    public void changeOneLesson(String day, String pair, String subject, String schclass) {
        Map<String, String> param = new HashMap<>();
        param.put("subject", subject);
        param.put("day", day);
        param.put("schclass", schclass);
        jdbcTemplate.update("UPDATE timeclass SET " + pair + " = :subject WHERE day = :day AND schclass = :schclass",
                param);
    }

    public Map<String, String> getExamMarksForClass(String schclass, String subject){
        Map<String,Object> param = new HashMap<>();
        Map<String,String> exams = new HashMap<>();
        param.put("subject", subject);
        for (User user:getUsersInClass(schclass)){
            param.put("id",user.getId());
            if (jdbcTemplate.queryForObject(
                    "SELECT exam FROM subjects WHERE id=:id AND subject = :subject",param,String.class).equals("0")){
                exams.put(user.getLogin(),"");
            }else {
                exams.put(user.getLogin(),jdbcTemplate.queryForObject(
                        "SELECT exam FROM subjects WHERE id=:id AND subject = :subject",param,String.class));
            }
        }
        return exams;
    }

    public void addNewClass(String newclass){
        Map<String,Object> param = new HashMap<>();
        param.put("schclass",newclass);
        jdbcTemplate.update("INSERT INTO timeclass (day,schclass) VALUES ('Monday',:schclass)",param);
        jdbcTemplate.update("INSERT INTO timeclass (day,schclass) VALUES ('Thuesday',:schclass)",param);
        jdbcTemplate.update("INSERT INTO timeclass (day,schclass) VALUES ('Wednesday',:schclass)",param);
        jdbcTemplate.update("INSERT INTO timeclass (day,schclass) VALUES ('Thursday',:schclass)",param);
        jdbcTemplate.update("INSERT INTO timeclass (day,schclass) VALUES ('Friday',:schclass)",param);
        jdbcTemplate.update("INSERT INTO timeclass (day,schclass) VALUES ('Saturday',:schclass)",param);
        jdbcTemplate.update("INSERT INTO timeclass (day,schclass) VALUES ('Sunday',:schclass)",param);

    }
}
