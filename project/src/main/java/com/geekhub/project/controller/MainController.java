package com.geekhub.project.controller;

import com.geekhub.project.dto.User;
import com.geekhub.project.services.DbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class MainController {
    @Autowired
    private DbService dbService;

    @RequestMapping("nice")
    public String main() {
        dbService.addUser(new User("anton", "step", "login", "ROLE_USER", "login", "9-A"));
        return "index";
    }

    @GetMapping("/cool")
    public String cool(Model model) {
        //model.addAttribute("message", dbService.getAllUsers());

        return "example";
    }

    @GetMapping("/home")
    public String home() {
        return "home";
    }

    @GetMapping("/login")
    public String login(@RequestParam(required = false) Map<String, String> map, Model model) {
        if (map.get("surname") != (null)) {
            dbService.addUser(new User(
                    map.get("name"),
                    map.get("surname"),
                    map.get("password"),
                    "ROLE_USER",
                    map.get("login"),
                    map.get("classorsubject")));
        }
        model.addAttribute("error", "");
        if (map.get("fall") != null && map.get("fall").equals("true")) {
            model.addAttribute("error", "Incorrect login or password");
        }
        return "login";
    }

//    @GetMapping("/admin")
//    public String admin() {
//        return "admin";
//    }

    @GetMapping("/school")
    public String school(Model model, HttpServletRequest req) {
        List<String> list = dbService.getSubjects(dbService.getUserByLogin(req.getUserPrincipal().getName()).getClassorsubject());
        //dbService.getAllUsers();
        model.addAttribute("subjects", list);
        return "school/school";
    }

    @GetMapping("/school/timetable")
    public String timeTable(Model model, HttpServletRequest req) {
        User user = dbService.getUserByLogin(req.getUserPrincipal().getName());
        List<List<String>> list = dbService.getTimeTable(user.getClassorsubject());
        model.addAttribute("list", list);
        return "school/timetable";
    }

    @GetMapping("/school/exams")
    public String exams(Model model, HttpServletRequest req) {
        User user = dbService.getUserByLogin(req.getUserPrincipal().getName());
        List<String> lessons = dbService.getSubjects(user.getClassorsubject());
        List<String> numbers = new ArrayList<>();
        Map<String, List<String>> map = new HashMap<>();
        for (String subject : lessons) {
            map.put(subject, dbService.getSevenMarks(dbService.getMarksOfUser(user.getClassorsubject(), subject, 21), user.getSurname()));
        }
        model.addAttribute("dates", dbService.getDatesExamForUser(user));
        model.addAttribute("list", lessons);
        model.addAttribute("map", map);
        return "school/examsmarks";
    }

    @GetMapping("/school/magazine")
    public String magazine(Model model, HttpServletRequest req, @RequestParam String subject) {
        User user = dbService.getUserByLogin(req.getUserPrincipal().getName());
        List<User> list = dbService.getUsersInClass(user.getClassorsubject());
        List<String> dates = dbService.getActualDates(dbService.getDatesClass(list.get(0).getClassorsubject(), subject));
        Map<String, List<String>> map = dbService.getMarksWithSemester(dbService.getMarksOfUser(
                list.get(0).getClassorsubject(), subject, dates.size()));
        model.addAttribute("hw", dbService.getHomeworksOfClass(user.getClassorsubject(), subject));
        model.addAttribute("names", dbService.getNamesHomeworksOfClass(user.getClassorsubject(), subject));
        model.addAttribute("exams", dbService.getExamMarksForClass(user.getClassorsubject(),subject));
        model.addAttribute("users", list);
        model.addAttribute("map", map);
        model.addAttribute("dates", dates);
        return "school/magazine";
    }

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @GetMapping("/teacher")
    public String teacher(HttpServletRequest req, Model model) {
        User user = dbService.getUserByLogin(req.getUserPrincipal().getName());
        model.addAttribute("list", dbService.getClasessTeachers(user));
        return "teacher/teacher";
    }

    @GetMapping("/teacher/magazine")
    public String teacherMagazine(Model model, HttpServletRequest req, @RequestParam(required = false) String schclass,
                                  @RequestParam(required = false) Map<String, String> params) {
        if (schclass == null || schclass.equals("")) {
            schclass = (String) req.getSession().getAttribute("schclass");
        } else {
            req.getSession().setAttribute("schclass", schclass);
        }
        User user = dbService.getUserByLogin(req.getUserPrincipal().getName());
        List<User> list = dbService.getUsersInClass(schclass);
        List<String> dates = dbService.getActualDates(dbService.getDatesClass(schclass, user.getClassorsubject()));
        dbService.whatDo(params, user.getClassorsubject(), schclass, dates.size());
        List<String> newdates = dbService.getActualDates(dbService.getDatesClass(schclass, user.getClassorsubject()));
        Map<String, List<String>> map = dbService.getMarksWithSemester(dbService.getMarksOfUser(
                schclass, user.getClassorsubject(), newdates.size()));
        model.addAttribute("hw", dbService.getHomeworksOfClass(
                (String) req.getSession().getAttribute("schclass"), user.getClassorsubject()));
        model.addAttribute("names", dbService.getNamesHomeworksOfClass(
                (String) req.getSession().getAttribute("schclass"), user.getClassorsubject()));
        model.addAttribute("numbers", dbService.getNumbers());
        model.addAttribute("users", list);
        model.addAttribute("map", map);
        model.addAttribute("exams", dbService.getExamMarksForClass(schclass,user.getClassorsubject()));
        model.addAttribute("dates", newdates);
        return "teacher/magazine";
    }

    @GetMapping("/teacher/exams")
    public String teacherExams(Model model, HttpServletRequest req, @RequestParam(required = false) Map<String, String> map) {
        User user = dbService.getUserByLogin(req.getUserPrincipal().getName());
        dbService.updateDateExam(map, user.getClassorsubject());
        model.addAttribute("list", dbService.getClasessTeachers(user));
        model.addAttribute("dates", dbService.getExamDates(dbService.getClasessTeachers(user),
                user.getClassorsubject()));
        return "teacher/exams";
    }

    @GetMapping("/admin")
    public String admin() {
        return "admin/admin";
    }

    @GetMapping("/admin/users")
    public String users(@RequestParam(required = false) String login, Model model,
                        @RequestParam(required = false) Map<String, String> map,
                        HttpServletRequest req) {
        if (login != null && !login.equals("")) {
            dbService.updateUser(map, login);
        }

        model.addAttribute("allusers", dbService.getAllUsers());
        return "admin/users";
    }

    @GetMapping("")
    public String startPage() {
        return "main";
    }

    @GetMapping("/teacher/timetable")
    public String timeTableTeacher(Model model, HttpServletRequest req) {
        User user = dbService.getUserByLogin(req.getUserPrincipal().getName());
        List<List<String>> list = dbService.getTimeTable(user.getLogin());
        model.addAttribute("list", list);
        return "school/timetable";
    }

    @GetMapping("/admin/timetable")
    public String timeTableAdmin(Model model, @RequestParam(required = false) String subject,
                                 @RequestParam(required = false) String day,
                                 @RequestParam(required = false) String pair,
                                 HttpServletRequest req) {
        if (day != null) {
            dbService.changeOneLesson(day, pair, subject, (String) req.getSession().getAttribute("change"));
        }
        model.addAttribute("map", dbService.sortClass(dbService.getAllClasess()));
        return "admin/timetable";
    }

    @GetMapping("/admin/timetable/change")
    public String changeTableAdmin(Model model, @RequestParam String schclass, @RequestParam String teacher,
                                   HttpServletRequest req, @RequestParam String newclass) {
        if (schclass != null && !schclass.equals("")) {
            if (teacher != null && !teacher.equals("")) {
                model.addAttribute("link", "/admin/timetable");
                model.addAttribute("message", "Pls choose one case");
                return "errorpage";
            } else {
                req.getSession().setAttribute("change", schclass);
                model.addAttribute("map", dbService.getTimeTable(schclass));
                model.addAttribute("subjects", dbService.getAllSubjectsForClass(schclass));
            }
        } else if (teacher != null && !teacher.equals("")) {
            req.getSession().setAttribute("change", teacher);
            model.addAttribute("map", dbService.getTimeTable(teacher));
            model.addAttribute("subjects", dbService.getClasessTeachers(dbService.getUserByLogin(teacher)));
        } else if (newclass!=null&&!newclass.equals("")){
            dbService.addNewClass(newclass);
        } else {
            model.addAttribute("link", "/admin/timetable");
            model.addAttribute("message", "Pls choose one case");
            return "errorpage";
        }
        return "admin/change";
    }

    @GetMapping("/admin/changesub")
    public String changeSubject(Model model, @RequestParam(required = false) String schclass,
                                @RequestParam(required = false) String subject,
                                @RequestParam(required = false) String teacher,
                                @RequestParam(required = false) String action) {
        if (schclass != null) {
            if (action.equals("add")) {
                dbService.addSubjectInClass(schclass, subject);
                dbService.addClassTeacher(schclass, teacher);
            } else {

            }
        }
        model.addAttribute("classes", dbService.sortClass(dbService.getAllClasess()).get("classes"));
        model.addAttribute("teachers", dbService.getAllTeachers());
        return "/admin/changesub";
    }

    @GetMapping("/error")
    public String error(Model model){
        model.addAttribute("message","access is denied");
        return "main";
    }
}

