//package com.geekhub.project.controller;
//
//import org.h2.jdbc.JdbcSQLException;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseStatus;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//
//@ControllerAdvice(basePackages = {"com.geekhub.lessons"}, assignableTypes = {MainController.class})
//public class ControllerExceptionHandler {
//    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
//    @ExceptionHandler(JdbcSQLException.class)
//    public ModelAndView exceptionHandler(Exception ex, HttpServletRequest req){
//        ModelAndView mav = new ModelAndView("incorrectdata");
//        mav.addObject("message", req.getRequestURI());
//        //System.out.println(req.getRequestURI()+"\n\n\n\n\n\n\n\n\n");
//        return mav;
//    }
//}
