create table homeworks (
  namehw VARCHAR(255) UNIQUE,
  schclass varchar(255),
  subject VARCHAR(255),
  hw VARCHAR(255) DEFAULT '-'
)