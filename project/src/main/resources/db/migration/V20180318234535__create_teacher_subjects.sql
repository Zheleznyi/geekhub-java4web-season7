create table teachclasses (
  id integer UNIQUE,
  surname varchar(255),
  login varchar(255) UNIQUE,
  subject VARCHAR(255),
  one BOOLEAN DEFAULT FALSE,
  two BOOLEAN DEFAULT FALSE,
  three BOOLEAN DEFAULT FALSE,
  four BOOLEAN DEFAULT FALSE,
  five BOOLEAN DEFAULT FALSE,
  six BOOLEAN DEFAULT FALSE,
  seven BOOLEAN DEFAULT FALSE,
  eight BOOLEAN DEFAULT FALSE,
  nine BOOLEAN DEFAULT FALSE,
  ten BOOLEAN DEFAULT FALSE,
  eleven BOOLEAN DEFAULT FALSE,
  primary key (id)
)