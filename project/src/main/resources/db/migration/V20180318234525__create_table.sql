create table users (
  id integer UNIQUE,
  name varchar(255),
  surname varchar(255),
  password varchar(255),
  role varchar(255) DEFAULT 'ROLE_USER',
  login varchar(255) UNIQUE,
  classorsubject VARCHAR(255),
  primary key (id)
)