INSERT INTO users(id,name, surname, password, role, login,classorsubject)
VALUES
  (1,'Vadim', 'Moskalov', '12dea96fec20593566ab75692c9949596833adc9' , 'ROLE_USER', 'user','9-A'),
  (2,'Teacher', 'teacher','4a82cb6db537ef6c5b53d144854e146de79502e8', 'ROLE_TEACHER', 'teacher','math'),
  (3,'Admin', 'Admin' , 'd033e22ae348aeb5660fc2140aec35850c4da997', 'ROLE_ADMIN', 'admin','admin'),
  (4,'Anton', 'Stepanov', '12dea96fec20593566ab75692c9949596833adc9' , 'ROLE_USER', 'user1','9-B'),
  (5,'Oleg', 'Subotin', '12dea96fec20593566ab75692c9949596833adc9' , 'ROLE_USER', 'user2','9-C'),
  (6,'Alexandr', 'Usenko', '12dea96fec20593566ab75692c9949596833adc9' , 'ROLE_USER', 'user3','9-A'),
  (7,'Dmytro', 'Hutsulenko', '12dea96fec20593566ab75692c9949596833adc9' , 'ROLE_USER', 'user4','9-A'),
  (8,'Serhiy', 'Pavlov', '12dea96fec20593566ab75692c9949596833adc9' , 'ROLE_USER', 'user5','9-A'),
  (9,'Artem', 'Zimin', '12dea96fec20593566ab75692c9949596833adc9' , 'ROLE_USER', 'user6','9-A')