INSERT INTO subjects(id,name, surname, schclass,subject)
VALUES
  (1,'Vadim', 'Moskalov', '9-A','math'),
  (4,'Anton', 'Stepanov', '9-B','math'),
  (5,'Oleg', 'Subotin', '9-C','math'),
  (6,'Alexandr', 'Usenko', '9-A','math'),
  (7,'Dmytro', 'Hutsulenko', '9-A','math'),
  (8,'Serhiy', 'Pavlov', '9-A','math'),
  (9,'Artem', 'Zimin', '9-A','math'),
  (1,'Vadim', 'Moskalov', '9-A','culture'),
  (1,'Vadim', 'Moskalov', '9-A','litr'),
  (4,'Anton', 'Stepanov', '9-B','litr'),
  (5,'Oleg', 'Subotin', '9-C','litr'),
  (6,'Alexandr', 'Usenko', '9-A','litr'),
  (7,'Dmytro', 'Hutsulenko', '9-A','litr'),
  (8,'Serhiy', 'Pavlov', '9-A','litr'),
  (9,'Artem', 'Zimin', '9-A','litr'),
  (1,'Vadim', 'Moskalov', '9-A','chemistry'),
  (4,'Anton', 'Stepanov', '9-B','chemistry'),
  (5,'Oleg', 'Subotin', '9-C','chemistry'),
  (6,'Alexandr', 'Usenko', '9-A','chemistry'),
  (7,'Dmytro', 'Hutsulenko', '9-A','chemistry'),
  (8,'Serhiy', 'Pavlov', '9-A','chemistry'),
  (9,'Artem', 'Zimin', '9-A','chemistry'),
  (1,'Vadim', 'Moskalov', '9-A','physics'),
  (4,'Anton', 'Stepanov', '9-B','physics'),
  (5,'Oleg', 'Subotin', '9-C','physics'),
  (6,'Alexandr', 'Usenko', '9-A','physics'),
  (7,'Dmytro', 'Hutsulenko', '9-A','physics'),
  (9,'Artem', 'Zimin', '9-A','physics'),
  (8,'Serhiy', 'Pavlov', '9-A','physics')
