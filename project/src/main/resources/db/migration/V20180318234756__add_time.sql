INSERT INTO timeclass(day,one,two,three,schclass)
VALUES
  ('Monday','math','chemistry','litr','9-A'),
  ('Tuesday','litr','math','chemistry','9-A'),
  ('Wednesday','chemistry','math','physics','9-A'),
  ('Thursday','math','physics','chemistry','9-A'),
  ('Friday','litr','math','math','9-A'),
  ('Saturday','math','chemistry','chemistry','9-A'),
  ('Sunday','-','-','-','9-A'),
  ('Monday','math','chemistry','litr','9-C'),
  ('Tuesday','litr','math','chemistry','9-C'),
  ('Wednesday','chemistry','math','physics','9-C'),
  ('Thursday','math','physics','chemistry','9-C'),
  ('Friday','litr','math','math','9-C'),
  ('Saturday','math','chemistry','chemistry','9-C'),
  ('Monday','math','chemistry','litr','9-B'),
  ('Tuesday','litr','math','chemistry','9-B'),
  ('Wednesday','chemistry','math','physics','9-B'),
  ('Thursday','math','physics','chemistry','9-B'),
  ('Friday','litr','math','math','9-B'),
  ('Saturday','math','chemistry','chemistry','9-B'),
  ('Monday','9-A','9-A','9-C','teacher'),
  ('Thuesday','-','9-C','-','teacher'),
  ('Wednesday','9-C','9-A','-','teacher'),
  ('Thursday','-','9-A','-','teacher');
