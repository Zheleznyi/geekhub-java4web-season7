

public class Beer implements Comparable {
    private String whatIsBeer;

    Beer(String beer){
        this.whatIsBeer = beer;
    }

    boolean isTasty() {
        boolean status = false;
        if (whatIsBeer.equals("Nice beer")||whatIsBeer.equals("Good beer")){
            status = true;
        }
        return status;
    }

    @Override
    public String toString() {
        return this.whatIsBeer;
    }

    public  int toInt(){
        int status = 0;
        if (whatIsBeer.equals("Nice beer")||whatIsBeer.equals("Good beer")){
            status = 1;
        }
        return status;
    }


    @Override
    public int compareTo(Object o) {
        return whatIsBeer.compareTo(o.toString());
    }
}

