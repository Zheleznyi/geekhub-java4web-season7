import java.util.List;
import java.util.function.*;

public class Demo {
    public static void main(String[] args) {
        CollectionOperattions coStreamRelease = new COStreamRelease();
        Supplier<Beer> sup = () -> new Beer("Good beer");
        Supplier<Beer> sup1 = () -> new Beer("Nice beer");
        Supplier<Beer> sup2 = () -> new Beer("Bad beer");
        Consumer<Beer> consumer = System.out::println;
        Function<Beer, String> function = Beer::toString;
        Function<Beer, Integer> function1 = Beer::toInt;
        BinaryOperator<Integer> accumulator = (n, m) -> n + 1;

        Predicate<Beer> predicate = Beer::isTasty;

        List<Beer> list = coStreamRelease.fill(sup, 2);
        list.addAll(coStreamRelease.fill(sup1, 4));
        list.addAll(coStreamRelease.fill(sup2, 3));
        System.out.println(list.size());
        System.out.println("List:");
        for (Beer beer : list) {
            System.out.print(beer + ", ");
        }
        System.out.println();

        List<Beer> list1 = coStreamRelease.filter(list, predicate);
        System.out.println("List1 after filter:");
        for (Beer beer : list1) {
            System.out.print(beer + ", ");
        }
        System.out.println();

        System.out.println("Have list tasty beer: " + coStreamRelease.anyMatch(list, predicate));
        System.out.println("Have list all tasty beer: " + coStreamRelease.allMatch(list, predicate));
        System.out.println("Have list1 all tasty beer: " + coStreamRelease.allMatch(list1, predicate));
        System.out.println("Have list all bad beer: " + coStreamRelease.noneMatch(list, predicate));

        List<String> strings = coStreamRelease.map(list, function);
        System.out.println("List after function" + strings);

        System.out.println("Distinct list: " + coStreamRelease.distinct(list));

        coStreamRelease.forEach(list, consumer);

        System.out.println(list);

        System.out.println("Group list: " + coStreamRelease.groupBy(list, function));

        System.out.println("To map list: " + coStreamRelease.toMap(list, function, function1, accumulator));

        System.out.println("Partition list: " + coStreamRelease.partitionBy(list, predicate));


    }
}
