import exceptions.InvalidDataException;


import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@WebServlet(urlPatterns = "/session", name = "name")
public class Hello extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String out;
        String action = req.getParameter("action");
        if (action == null) {
            action = " ";
        }
        switch (action) {
            case "add":
                out = add(req);
                break;
            case "update":
                out = add(req);
                break;
            case "invalidate":
                out = invalidate(req);
                break;
            case "delete":
                out = remove(req);
                break;
            default:
                out = "Please choose case";
                break;
        }
        resp.getWriter().write("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                "\n" +
                "<form action=\"/session\" method=\"get\">\n" +
                "  Action:<br>\n" +
                "  <select name=\"action\">\n" +
                "  <option value=\"\"></option>\n" +
                "  <option value=\"add\">Add</option>\n" +
                "  <option value=\"update\">Update</option>\n" +
                "  <option value=\"invalidate\">Invalidate</option>\n" +
                "  <option value=\"delete\">Remove</option>\n" +
                "  </select>\n" +
                "  </br>\n" +
                "  <span style=\"color:black;\">Name:</span></br>\n" +
                "  <input type=\"text\" name=\"name\">\n" +
                "  <br>\n" +
                "  Value:<br>\n" +
                "  <input type=\"text\" name=\"value\">\n" +
                "  <br><br>\n" +
                "  <input type=\"submit\" value=\"Submit\">\n" +
                "</form>\n" +
                "\n" +
                "<hr>\n" +
                out +
                "\n" +
                "</body>\n" +
                "</html>\n");
    }

    private String add(HttpServletRequest req) throws InvalidDataException {
        Map<String, String> map;
        if ((req.getSession().getAttribute("map") == null)) {
            map = new HashMap<>();
        } else {
            map = (Map<String, String>) req.getSession().getAttribute("map");
        }
        String out = "";
        if (req.getParameter("name") == null || req.getParameter("name").equals("")) {
            throw new InvalidDataException("");
        } else {
            String name = req.getParameter("name");
            String value = req.getParameter("value");
            map.put(name, value);
        }
        req.getSession().setAttribute("map", map);
        Set<String> names = map.keySet();
        for (String n : names) {
            out = out + n + ":" + map.get(n) + "<br>\n";
        }
        return out;
    }

    private String remove(HttpServletRequest req) {
        Map<String, String> map;
        if ((req.getSession().getAttribute("map") == null)) {
            map = new HashMap<>();
        } else {
            map = (Map<String, String>) req.getSession().getAttribute("map");
        }
        String out = "";
        if (req.getParameter("name") == null || req.getParameter("name").equals("")) {
            throw new InvalidDataException("");
        } else {
            String name = req.getParameter("name");
            map.remove(name);
        }
        req.getSession().setAttribute("map", map);
        Set<String> names = map.keySet();
        for (String n : names) {
            out = out + n + ":" + map.get(n) + "<br>\n";
        }
        return out;
    }

    private String invalidate(HttpServletRequest req) {
        req.getSession().invalidate();
        return "no data";
    }
}