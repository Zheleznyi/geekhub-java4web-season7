
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/error", name = "error")
public class Error extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws  IOException {
        resp.getWriter().write("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                "\n" +
                "<hr>\n" +
                "incorrect data\n" +
                "<form action=\"/session\" method=\"LINK\">"+
                "<input type=\"submit\" value=\"Return\">\n" +
                "</form>\n" +
                "</body>\n" +
                "</html>\n");
    }
}
