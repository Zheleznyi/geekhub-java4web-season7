package geek;


import java.io.Serializable;

public class User implements Serializable, Comparable<User>{
    private static final long serialVersionUID = -2017;

    private int id;
    private String name;
    private int accessLevel;
    private String favoriteEncoding;


    public User(int id, String name, int accessLevel, String favoriteEncoding){
        this.id = id;
        this.name = name;
        this.accessLevel = accessLevel;
        this.favoriteEncoding = favoriteEncoding;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public String getFavoriteEncoding() {
        return favoriteEncoding;
    }



    @Override
    public String toString() {
        return "com.user{ id= " + id + ", name = " + name + ", favoriteEncoding = " + favoriteEncoding + ", accessLevel = "
                + accessLevel + "}";
    }

    @Override
    public int compareTo(User o) {
        String ids = accessLevel + "";
        return ids.compareTo(o.accessLevel + "");
    }
}
