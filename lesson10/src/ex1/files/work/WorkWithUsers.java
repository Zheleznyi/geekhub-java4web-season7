package ex1.files.work;

import geek.User;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class WorkWithUsers {
    public int allUsersCount(List<User> users) {
        return users.size();
    }


    public String mostFavoriteEncoding(List<User> users) {
        Map<String, List<User>> map = users.stream().collect(Collectors.groupingBy(com.user::getFavoriteEncoding));
        int value = 0;
        String mfe = "";
        for (List<User> list : map.values()){
            if (list.size()> value){
                value = list.size();
                for (String s : map.keySet()){
                    if (map.get(s).equals(list)){
                        mfe = s;
                    }
                }
            }
        }
        return mfe;
    }

    public List<User> averageLevel(List<User> users) {
        int level = 0;
        for (User u : users) {
            level = level + u.getAccessLevel();
        }
        level = level / allUsersCount(users);
        List<User> users1 = new ArrayList<>();
        for (User u : users) {
            if (u.getAccessLevel() > level) {
                users1.add(u);
            }
        }
        return users1;
    }

    public int UserWithLongestName(List<User> users) {
        int length = 0;
        int id = 0;
        for (User u : users) {
            if (u.getName().length() > length) {
                length = u.getName().length();
                id = u.getId();
            }
        }
        return id;
    }

    public void writeNamesInFile1(List<User> users) {
        String names = "";
        for (User u : users) {
            names = names + u.getName() + "\r\n";
        }
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("D:/Documents" +
                "/idea/geekhub-java4web-season7/lesson10/src/ex1/files/file1.txt"), "UTF-8")))  {
            bw.write(names);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeNameInFile2(List<User> users){
        String names = "";
        for (User u : users) {
            names = names + u.getName() + "\r\n";
        }
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("D:/Documents" +
                "/idea/geekhub-java4web-season7/lesson10/src/ex1/files/file2.txt"), "UTF-16")))  {
            bw.write(names);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
