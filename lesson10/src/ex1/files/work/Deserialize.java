package ex1.files.work;

import geek.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Deserialize {


    public List<User> getUsers(File dataFile){
        List<User> users = new ArrayList<>();
        try (FileInputStream fis = new FileInputStream(dataFile);
             BufferedInputStream bis = new BufferedInputStream(fis);
             ObjectInputStream in = new ObjectInputStream(bis)) {
            while (true) {
                Object object = in.readObject();
                if (object instanceof User) {
                    users.add((User) object);
                }
            }
        } catch (EOFException e){
            return users;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return users;
    }




}
