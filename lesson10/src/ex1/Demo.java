package ex1;

import ex1.files.work.Deserialize;
import ex1.files.work.WorkWithUsers;
import geek.User;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) {
//        String url = "https://onedrive.live.com/?authkey=%21APIsnqwyvYrfSqI&cid=21B0EF620FFF3801&id=21B0EF620FFF3801%21" +
//                "121236&parId=21B0EF620FFF3801%21118829&action=locate";
//
//        Downloader df = new Downloader();
//
//        try {
//            df.download(url, "/Documents/idea/geekhub-java4web-season7/lesson10/src/ex1/files/users.zip");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        ZipUtils unpackzip = new ZipUtils();
//
//        File file = new File("/Documents/idea/geekhub-java4web-season7/lesson10/src/ex1/files/users.zip");
//        if (!file.exists() || !file.canRead()) {
//            System.out.println("File cannot be read");
//            return;
//        }
//
//        try {
//            ZipFile zip = new ZipFile("/Documents/idea/geekhub-java4web-season7/lesson10/src/ex1/files/users.zip");
//            Enumeration entries = zip.entries();
//
//            while (entries.hasMoreElements()) {
//                ZipEntry entry = (ZipEntry) entries.nextElement();
//                System.out.println(entry.getName());
//
//                if (entry.isDirectory()) {
//                    new File(file.getParent(), entry.getName()).mkdirs();
//                } else {
//                    ZipUtils.unzip(zip.getInputStream(entry),
//                            new BufferedOutputStream(new FileOutputStream(
//                                    new File(file.getParent(), entry.getName()))));
//                }
//            }
//
//            zip.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        File files = new File("D:/Documents/idea/geekhub-java4web-season7/lesson10/src/ex1/UnzipAndWork" +
                "/users/other/users.dat");
//
        Deserialize des = new Deserialize();
        List<User> list = new ArrayList<>();
        list.addAll(des.getUsers(files));
//
//        //Collections.sort(list);
//        for (com.user com.user : list) {
//            System.out.println(com.user);
//        }
//
//        WorkWithUsers wwu = new WorkWithUsers();
//        System.out.println("All users count: " + wwu.allUsersCount(list));
//        System.out.println("Most favorite Encoding: " + wwu.mostFavoriteEncoding(list));
//        System.out.println("Target com.user ID: " + wwu.UserWithLongestName(wwu.averageLevel(list)));
//        wwu.writeNamesInFile1(list);
//        wwu.writeNameInFile2(list);

        WorkWithUsers www = new WorkWithUsers();

        www.mostFavoriteEncoding(list);
    }
}
