package ex1;

import java.lang.reflect.InvocationTargetException;

public class Demo {
    public static void main(String[] args) {
        CloneCreator cloneCreator = new CloneCreatorImpl();
        User user = new User("5","6");
        try {
            System.out.println(cloneCreator.clone(user));
        } catch ( CanNotCloneException | IllegalAccessException |
                NoSuchMethodException | InstantiationException | InvocationTargetException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
