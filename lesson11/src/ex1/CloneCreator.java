package ex1;

public interface CloneCreator<T extends CanBeCloned> {

    T clone(T t) throws IllegalAccessException, InstantiationException, CanNotCloneException;

}
