package ex1;

public class CanNotCloneException extends Exception {

    CanNotCloneException(String message){
        super(message);
    }
}
