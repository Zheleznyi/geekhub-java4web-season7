package ex1;

import java.lang.reflect.Field;

public class CloneCreatorImpl<T extends CanBeCloned> implements CloneCreator {

    @Override
    public CanBeCloned clone(CanBeCloned canBeCloned) throws IllegalAccessException,
            InstantiationException, CanNotCloneException {
        Class user = CanBeCloned.class;
        Field[] fields = user.getDeclaredFields();
        for (Field f : fields) {
            if (f.isAnnotationPresent(Ignore.class)){
                throw new CanNotCloneException("Clone is unpossible");
            }
        }
        T user1 = (T) user.newInstance();
        return user1;
    }
}

