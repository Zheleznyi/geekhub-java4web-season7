package ex2;

public interface ChangeSetExtractor<T> {

    ChangeSet extract(T oldObject, T newObject);
}
