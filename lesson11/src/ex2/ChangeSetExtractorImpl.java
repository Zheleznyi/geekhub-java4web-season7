package ex2;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class ChangeSetExtractorImpl<T> implements ChangeSetExtractor {
    @Override
    public ChangeSet extract(Object oldObject, Object newObject) {
        ChangeSet changeSet1 = new ChangeSet();
        Class o = oldObject.getClass();
        Class n = newObject.getClass();
        Field[] fieldsO = o.getDeclaredFields();
        Field[] fieldsN = n.getDeclaredFields();
        Class changeSet = ChangeSet.class;
        Map<String, Object> oldFields = new HashMap<>();
        Map<String, Object> newFields = new HashMap<>();
        boolean hasChanges = false;
        for (int i = 0; i < fieldsO.length; i++) {
            try {
                if (!fieldsO[i].get(oldObject).equals(fieldsN[i].get(newObject))) {
                    oldFields.put(fieldsO[i].getName(), fieldsO[i].get(oldObject));
                    newFields.put(fieldsN[i].getName(), fieldsN[i].get(newObject));
                    hasChanges = true;
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        if (hasChanges) {
            changeSet1.setNewFields(newFields);
            changeSet1.setOldFields(oldFields);
        }


        return changeSet1;
    }
}
