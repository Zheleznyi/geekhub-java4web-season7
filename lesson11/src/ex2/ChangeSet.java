package ex2;


import java.util.HashMap;
import java.util.Map;

public class ChangeSet {
    Map<String, Object> oldFields;
    Map<String, Object> newFields;

    public Map<String, Object> getNewFields() {
        return newFields;
    }

    public Map<String, Object> getOldFields() {
        return oldFields;
    }

    public void setNewFields(Map<String, Object> newFields) {
        this.newFields = newFields;
    }

    public void setOldFields(Map<String, Object> oldFields) {
        this.oldFields = oldFields;
    }
}
