package ex2;

import ex1.User;

public class Demo {
    public static void main(String[] args) {
        User user = new User("u","1");
        User user1 = new User("t","2");
        ChangeSetExtractor changeSetExtractor = new ChangeSetExtractorImpl();
        System.out.println(changeSetExtractor.extract(user, user1).oldFields.get("login"));
        System.out.println(changeSetExtractor.extract(user, user1).newFields.get("login"));

    }
}
