package ex2;

import java.util.Date;

public class Demo {
    public static void main(String[] args) {
        TM tm = new TM();

        tm.addTask(new Date(2017,1,30),new ex2.Task("work","call",
                new Date(2017,12,30)));
        tm.addTask(new Date(2017,3,30),new ex2.Task("rail","event",
                new Date(2017,11,30)));
        tm.addTask(new Date(2017,2,30),new ex2.Task("day","call",
                new Date(2017,10,30)));
        tm.addTask(new Date(2017,2,30),new ex2.Task("day","call",
                new Date(2017,10,30)));
        tm.addTask(new Date(2017,2,30),new ex2.Task("day","call",
                new Date(2017,10,30)));
        tm.addTask(new Date(2017,2,30),new ex2.Task("day","call",
                new Date(2017,10,30)));

        System.out.println("Categories: ");

        for (String t : tm.getCategories()){
            System.out.print(t + ", ");
        }
        System.out.println();
        System.out.println("Tasks in category day:");

        for (Task task: tm.getTasksbyCategory().get("day")){
            System.out.print(task + ", ");
        }

        System.out.println();
        System.out.println("Tasks in category day:");

        for (Task task: tm.getListTasksbyCategory("day")){
            System.out.print(task + ", ");
        }




    }
}
