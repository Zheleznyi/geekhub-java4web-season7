package ex2;



import java.util.*;

public class TM implements TaskManager {
    private List<Date> dateList = new ArrayList<>();
    private List<ex2.Task> tasksList = new ArrayList<>();


    public void addTask(Date date, ex2.Task task) {
        dateList.add(date);
        tasksList.add(task);
    }

    @Override
    public void removeTask(Date date) {
        int i = dateList.indexOf(date);
        dateList.remove(date);
        tasksList.remove(i);
    }

    @Override
    public Collection<String> getCategories() {
        Collection<String> col = new ArrayList<>();
        for (ex2.Task task : tasksList) {
            int p = 0;
            for (int i = 0; i < tasksList.size(); i++) {
                if (col.contains(task.getCategory())) {
                    i = tasksList.size();
                } else {
                    p = p + 1;
                }
                if (p == tasksList.size())
                    col.add(task.getCategory());
            }

        }
        return col;
    }

    @Override
    public Map<String, List<ex2.Task>> getTasksbyCategory() {
        Map<String ,List<ex2.Task>> map3 = new HashMap<>();
        for (String category : getCategories()){
            map3.put(category, getListTasksbyCategory(category));
        }
        return map3;
    }

    @Override
    public List<ex2.Task> getListTasksbyCategory(String category) {
        List<ex2.Task> list = new ArrayList<>();
        for (Task task : tasksList){
            if (task.getCategory().equals(category)){
                list.add(task);
            }
        }
        return list;
    }

    @Override
    public List<ex2.Task> getTasksForToday() {
        List<ex2.Task> list = new ArrayList<>();
        Date date = new Date();
        for (Task task : tasksList){
            if (task.getDate().getDay() == (date.getDay())){
                list.add(task);
            }
        }
        return list;
    }
}
