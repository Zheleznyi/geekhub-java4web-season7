package ex2;

import java.util.Date;

public class Task {
    private String category;
    private String description;
    private Date date;

   Task(String category, String description, Date date){
       this.category = category;
       this.description = description;
       this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        return description;
    }
}
