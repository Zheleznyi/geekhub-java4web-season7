package ex2;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface TaskManager {
    void addTask(Date date, ex2.Task task);
    void removeTask(Date date);
    Collection<String> getCategories();

    Map<String, List<ex2.Task>> getTasksbyCategory();
    List<ex2.Task> getListTasksbyCategory(String category);
    List<ex2.Task> getTasksForToday();

}
