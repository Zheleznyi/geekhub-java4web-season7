package ex1;

public class Car implements Comparable<Car> {
    private final String brand;
    private final Integer length;
    private final String color;

    public Car(String car, Integer length, String color) {
        this.brand = car;
        this.length = length;
        this.color = color;
    }

    public String getBrand() {
        return brand;
    }

    public int getLength() {
        return length;
    }

    public String getColor() {
        return color;
    }

    @Override
    public int compareTo(Car o) {
        int compareCar = this.brand.compareTo(o.brand);
        int compareLength = this.length.compareTo(o.length);
        if (compareCar!=0) {
            return compareCar;
        } else if (compareLength != 0) {
            return compareLength;
        } else {
            return this.color.compareTo(o.color);
        }
    }

}
