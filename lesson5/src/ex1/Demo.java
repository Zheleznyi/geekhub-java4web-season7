package ex1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Demo {
    public static void main(String[] args) {
        List<Car> cars = new ArrayList<>();
        cars.add(new Car("BMW X5", 6, "black"));
        cars.add(new Car("BMW X5", 5, "black"));
        cars.add(new Car("Mercedes", 3, "yellow"));
        cars.add(new Car("Lincoln", 4, "black"));
        cars.add(new Car("BMW X6", 5, "yellow"));
        cars.add(new Car("Mercedes", 6, "black"));
        cars.add(new Car("BMW X5", 7, "yellow"));
        cars.add(new Car("BMW X5", 5, "yellow"));

        Collections.sort(cars);

        for (Car car : cars) {
            System.out.println(car.getBrand() + ", length: " + car.getLength() + ", color: " + car.getColor());
        }


    }


}
