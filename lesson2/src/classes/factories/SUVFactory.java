package classes.factories;

import classes.DriveType;
import classes.Transports.SUV;
import classes.parts.*;
import interfaces.Factory;

public class SUVFactory implements Factory {

    @Override
    public SUV create() {
        return new SUV(DriveType.FULL, "Jeep", new Accelerator(), new Engine(200, EngineType.DIESEL),
                new FrontWheel[]{new FrontWheel(), new FrontWheel()}, new RearWheel[]{new RearWheel(), new RearWheel()},
                new BrakePedal(), new GasTank(60));
    }
}
