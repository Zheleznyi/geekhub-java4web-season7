package classes.factories;

import classes.DriveType;
import classes.Transports.Coupe;
import classes.parts.*;
import interfaces.Factory;

public class CoupeFactory implements Factory {
    @Override
    public Coupe create() {
        return new Coupe(DriveType.REAR, "Mazda", new Accelerator(), new Engine(250, EngineType.DIESEL),
                new FrontWheel[]{new FrontWheel(), new FrontWheel()}, new RearWheel[]{new RearWheel(), new RearWheel()},
                new BrakePedal(), new GasTank(50));
    }
}
