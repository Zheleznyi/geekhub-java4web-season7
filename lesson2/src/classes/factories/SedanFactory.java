package classes.factories;

import classes.DriveType;
import classes.Transports.Sedan;
import classes.parts.*;
import interfaces.Factory;

public class SedanFactory implements Factory{

    public Sedan create() {
        return new Sedan(DriveType.FRONT, "VAZ 99", new Accelerator(), new Engine(150, EngineType.GAS),
                new FrontWheel[]{new FrontWheel(), new FrontWheel()}, new RearWheel[]{new RearWheel(), new RearWheel()},
                new BrakePedal(), new GasTank(40));
    }
}
