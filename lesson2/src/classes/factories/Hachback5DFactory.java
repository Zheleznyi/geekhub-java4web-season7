package classes.factories;

import classes.DriveType;
import classes.Transports.Hachback5doors;
import classes.parts.*;
import interfaces.Factory;

public class Hachback5DFactory implements Factory {
    @Override
    public Hachback5doors create(){
        return new Hachback5doors(DriveType.FRONT, "Toyota Corolla ", new Accelerator(), new Engine(160,
                EngineType.GAS), new FrontWheel[]{new FrontWheel(), new FrontWheel()}, new RearWheel[]
                {new RearWheel(), new RearWheel()},
                new BrakePedal(), new GasTank(45));
    }
}
