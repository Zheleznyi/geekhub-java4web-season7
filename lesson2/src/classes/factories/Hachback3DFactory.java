package classes.factories;

import classes.DriveType;
import classes.Transports.Hachback3doors;
import classes.parts.*;
import interfaces.Factory;

public class Hachback3DFactory implements Factory {
    @Override
    public Hachback3doors create(){
        return new Hachback3doors(DriveType.FRONT, "Ford Focus", new Accelerator(), new Engine(160,
                EngineType.GAS), new FrontWheel[]{new FrontWheel(), new FrontWheel()}, new RearWheel[]
                {new RearWheel(), new RearWheel()},
                new BrakePedal(), new GasTank(45));
    }


}

