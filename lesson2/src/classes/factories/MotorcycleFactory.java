package classes.factories;

import classes.DriveType;
import classes.Transports.Motorcycle;
import classes.parts.*;
import interfaces.Factory;

public class MotorcycleFactory implements Factory {
    public Motorcycle create() {
        return new Motorcycle(0,DriveType.FRONT, "BMX", new Accelerator(), new Engine(150, EngineType.GAS),
                new FrontWheel[]{new FrontWheel()}, new RearWheel[]{new RearWheel(), },
                new BrakePedal(), new GasTank(40));
    }
}
