package classes;

public enum DriveType {
    FULL, FRONT, REAR
}
