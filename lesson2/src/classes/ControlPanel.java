package classes;


public class ControlPanel {
    public void switchOn(Drivable drivable){
        drivable.getEngine().switchOn();
    }

    public void switchOff(Drivable drivable){
        drivable.getEngine().switchOff();
    }

    public void accelerate(Drivable drivable){
        drivable.getAccelerator().accelerate();
    }

    public void slowDown(Drivable drivable){
        drivable.getBrakePedal().brake();
    }

}
