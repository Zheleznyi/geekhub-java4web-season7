package classes.parts;

import interfaces.StatusAware;

public class GasTank implements StatusAware {
    private final int maxSize;
    private int currentSize;

    public GasTank(int maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    public String getStatus() {
        return String.valueOf(currentSize);
    }

    public void setCurrentSize(int currentSize) {
        if (currentSize < 0 || currentSize > maxSize) {
            throw new IllegalArgumentException("Wrong volume specified: " + currentSize);
        }
        this.currentSize = currentSize;
    }

    public int getMaxSize() {
        return maxSize;
    }

    @Override
    public String toString() {
        return currentSize + " of " + maxSize;
    }
}
