package classes.parts;

public enum EngineType {
    DIESEL, GAS, ELECTRIC
}
