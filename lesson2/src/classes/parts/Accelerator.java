package classes.parts;

import interfaces.StatusAware;

public class Accelerator implements StatusAware {
    private String status;


    @Override
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String toString() {
        return status;
    }

    public void accelerate() {
        status = "Used";
        System.out.println("Speed up!");
    }
}
