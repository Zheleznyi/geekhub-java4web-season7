package classes.parts;

import interfaces.Retarding;
import interfaces.Steared;

public class FrontWheel implements Steared, Retarding{

    @Override
    public void slowDown() {
        System.out.println("Stop");
    }

    @Override
    public void turnLeft() {
        System.out.println("Left");
    }

    @Override
    public void turRight() {
        System.out.println("Right");
    }
}
