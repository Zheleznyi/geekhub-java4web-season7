package classes.Transports;

import classes.parts.BrakePedal;
import classes.parts.GasTank;
import classes.parts.Engine;
import classes.parts.Accelerator;
import classes.parts.FrontWheel;
import classes.parts.RearWheel;
import classes.DriveType;

public class SUV extends Vehicle {
    public SUV(DriveType driveType, String name, Accelerator accelerator, Engine engine, FrontWheel[] front,
                 RearWheel[] rear, BrakePedal brakePedal, GasTank gasTank){
        super(4, driveType, name, accelerator, engine, front, rear, brakePedal, gasTank);
    }

}
