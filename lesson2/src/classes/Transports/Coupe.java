package classes.Transports;

import classes.DriveType;
import classes.parts.*;

public class Coupe extends Vehicle {
    public Coupe(DriveType driveType, String name, Accelerator accelerator, Engine engine, FrontWheel[] front,
                 RearWheel[] rear, BrakePedal brakePedal, GasTank gasTank){
        super(2, driveType, name, accelerator, engine, front, rear, brakePedal, gasTank);
    }

}
