package classes.Transports;

import classes.Drivable;
import classes.DriveType;
import classes.parts.*;

public class Motorcycle implements Drivable {
    protected final int numberOfDoors;
    protected final DriveType driveType;
    protected int weight;
    protected final byte numberOfWheels;
    protected final String name;
    protected final Accelerator accelerator;
    protected final Engine engine;
    protected final FrontWheel[] front;
    protected final RearWheel[] rear;
    protected final BrakePedal brakePedal;
    protected final GasTank gasTank;

    public Motorcycle(int numberOfDoors, DriveType driveType,
                      String name, Accelerator accelerator, Engine engine,
                      FrontWheel[] front, RearWheel[] rear, BrakePedal brakePedal, GasTank gasTank) {
        this.numberOfDoors = numberOfDoors;
        this.driveType = driveType;
        this.name = name;
        this.accelerator = accelerator;
        this.engine = engine;
        this.front = front;
        this.rear = rear;
        this.numberOfWheels = 2;
        this.brakePedal = brakePedal;
        this.gasTank = gasTank;
    }


    public byte getNumberOfWheels() {
        return numberOfWheels;
    }


    public Accelerator getAccelerator() {
        return accelerator;
    }


    public Engine getEngine() {
        return engine;
    }

    public int getNumberOfDoors() {
        return numberOfDoors;
    }

    public BrakePedal getBrakePedal(){
        return brakePedal;
    }

    public GasTank getGasTank() {
        return gasTank;
    }

    @Override
    public FrontWheel[] getFrontWheel() {
        return new FrontWheel[0];
    }

    @Override
    public RearWheel[] getRearWheel() {
        return new RearWheel[0];
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public DriveType getDriveType() {
        return driveType;
    }



    public String getName() {
        return name;
    }


    public FrontWheel[] getFrontWheels() {
        return front;
    }


    public RearWheel[] getRearWheels() {
        return rear;
    }

}
