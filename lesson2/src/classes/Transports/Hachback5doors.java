package classes.Transports;

import classes.DriveType;
import classes.parts.*;

public class Hachback5doors extends Vehicle {
    public Hachback5doors(DriveType driveType, String name, Accelerator accelerator, Engine engine, FrontWheel[] front,
                 RearWheel[] rear, BrakePedal brakePedal, GasTank gasTank){
        super(5, driveType, name, accelerator, engine, front, rear, brakePedal, gasTank);
    }

}
