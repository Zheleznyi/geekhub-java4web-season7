package classes.Transports;

import classes.DriveType;
import classes.parts.*;

public class Hachback3doors extends Vehicle {

    public Hachback3doors(DriveType driveType, String name, Accelerator accelerator, Engine engine, FrontWheel[] front,
                 RearWheel[] rear, BrakePedal brakePedal, GasTank gasTank){
        super(3, driveType, name, accelerator, engine, front, rear, brakePedal, gasTank);
    }

}
