package classes;


import classes.factories.*;
import interfaces.Factory;

public class Demo {
    public static void main(String[] args) {
        ControlPanel panel = new ControlPanel();
        for (Factory factory : factories()) {
            Drivable drivable = factory.create();
            System.out.println("Test Drive: " + drivable.getName());
            panel.switchOn(drivable);
            System.out.println(drivable.getEngine().getStatus());
            panel.accelerate(drivable);
            System.out.println(drivable.getAccelerator().getStatus());
            panel.slowDown(drivable);
            panel.switchOff(drivable);
            System.out.println("Test Drive Successfully Finished.");
        }
    }

    private static Factory[] factories() {
        return new Factory[] {
                new SedanFactory(),
                new SUVFactory(),
                new Hachback3DFactory(),
                new Hachback5DFactory(),
                new CoupeFactory(),
                new MotorcycleFactory()
        };
    }
}
