package classes;

import classes.parts.*;

public interface Drivable{
    abstract Accelerator getAccelerator();

    abstract Engine getEngine();

    abstract String getName();

    abstract int getNumberOfDoors();

    abstract FrontWheel[] getFrontWheel();

    abstract RearWheel[] getRearWheel();

    abstract BrakePedal getBrakePedal();

}
