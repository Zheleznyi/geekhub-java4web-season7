package interfaces;

import classes.Drivable;

public interface Factory {
    Drivable create();
}
