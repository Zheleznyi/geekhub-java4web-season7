package com.geekhub.lessons.ex2;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int number = Integer.parseInt(args[0]);
        int factorial = 1;
        if (number < 1) {
            System.out.println("Non positive Number");
        } else if (number > 10) {
            System.out.println("WARNING!!!!!!! Number very big, operation can take some time. You agree? (y/n) ");
            String t = in.next();
            if (t.equals("y")) {
                while (number > 0) {
                    factorial *= number;
                    number--;
                }
                System.out.println(factorial);
            }
        } else {
            while (number > 0) {
                factorial *= number;
                number--;
            }
            System.out.println(factorial);
        }
    }
}
