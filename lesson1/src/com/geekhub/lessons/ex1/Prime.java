package com.geekhub.lessons.ex1;


public class Prime {
    private void isSimple() {
        int remainder;

        System.out.println("Prime numbers less than 1000 in reverse order");
        for (int i = 1000; i > 0; i--) {

            remainder = i % 2;
            if (remainder == 0) {
                continue;
            }

            for (remainder = i / 2; remainder > 1; remainder--) {

                int reminder2 = remainder % 2;
                if (reminder2 == 0) {
                    continue;
                }

                reminder2 = i % remainder;

                if (reminder2 == 0) {
                    continue;
                }

                if (remainder == 3) {
                    System.out.println(i);
                }
            }

        }
        System.out.println("5" + "\n" + "3");
    }

    public static void main(String[] args) {

        Prime prime = new Prime();

        prime.isSimple();

    }
}


