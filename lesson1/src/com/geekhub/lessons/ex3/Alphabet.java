package com.geekhub.lessons.ex3;


public class Alphabet {
    public static void main(String[] args) {
        Alphabet alphabet = new Alphabet();
        alphabet.alphabet();

    }

    private void alphabet(){
        int i = 0;
        int index = 0;
        final int numberOfLetters = 26;
        final int translations = 32;
        char[] alphabetInLower = new char[numberOfLetters];
        char[] alphabet = new char[numberOfLetters];
        for (char b = 'A'; b <= 'Z'; b++) {

            alphabet[i] = b;
            alphabetInLower[i] = alphabet[i];
            i++;
        }
        for (char element : alphabet) {
            int number = (int) element + translations;
            alphabetInLower[index] = (char) number;
            index++;
        }
        //System.out.println(alphabet);
        System.out.println(alphabetInLower);
    }
}
