package com.geekhub.lessons.ex4;

import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        Game game = new Game();
        game.game();
    }

    private void game(){
        System.out.println("Let's play the game)) Input prime number");
        Scanner in = new Scanner(System.in);
        boolean i = true;
        while (i) {
            int number = in.nextInt();
            if (number == 3 || number == 1) {
                System.out.println("Congratulations, number is prime");
                i = false;
            } else {
                int twoness = number % 2;
                if (twoness == 0) {
                    System.out.println("You will dead in two days, if don't input prime number");
                } else {
                    for (int a = number / 2; a > 1; a--) {
                        int remainder = number % a;
                        if (remainder == 0) {
                            System.out.println("You will dead in two days, if don't input prime number");
                            break;
                        }
                        if (a == 2) {
                            System.out.println("Congratulations, number is prime");
                            i = false;
                        }
                    }
                }
            }
        }
    }
}