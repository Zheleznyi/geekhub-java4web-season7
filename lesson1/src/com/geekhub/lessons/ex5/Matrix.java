package com.geekhub.lessons.ex5;


import java.util.Scanner;

public class Matrix {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Input number of columns:");
        int cols = in.nextInt();
        System.out.println("Input number of rows:");
        int rows = in.nextInt();
        int[][] array = new int[rows][cols];
        double sqrt;
        int value;
        int xvalue = 1;
        double val;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i == 0) {
                    value = j + 1;
                } else {
                    value = array[i - 1][array[i].length - 1] + j + 1;
                }
                array[i][j] = value;
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                value = array[i][j];
                sqrt = Math.sqrt(value);
                val = sqrt;
                sqrt = sqrt % 1;
                if (sqrt == 0) {
                    System.out.println("Cell [" + (i + 1) + "," + (j + 1) + "] = " + value + " has integer root: "
                            + val);
                    xvalue = xvalue * (int) val;
                }
            }
        }
        System.out.println("Total: " + xvalue);
    }
}


