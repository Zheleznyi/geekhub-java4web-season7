package part1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Tests {
    List<String> q;
    Map<Integer,List<String>> a;

    public Map<Integer,List<String>> getA() {
        return a;
    }

    public List<String> getQ() {
        return q;
    }

    public void setA(Map<Integer,List<String>> a) {
        this.a = a;
    }

    public void setQ(List<String> q) {
        this.q = q;
    }
}
