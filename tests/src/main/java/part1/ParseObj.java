package part1;


import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParseObj {

    public Map<String, List<String>> getFile(String file) {
        List<String> questions = new ArrayList<>();
        List<String> answers = new ArrayList<>();
        Map<String, List<String>> map = new HashMap<>();
        String question = "";
        String answer = "";
        boolean writeQuestion = false;
        int newc = 758;
        try (FileReader fr = new FileReader(file)) {
            int c;
            while ((c = fr.read()) != -1) {
                if (writeQuestion) {
                    question = "";
                    if (isABCD(c)) {
                        newc = c;
                    }
                    if (c == 46) {
                        if (newc != 758) {
                            question = question + (char) newc + (char) c;
                            writeQuestion = false;
                            newc = 758;
                            answers.add(answer);
                            answer = "";
                        }
                    }
                    answer = answer + (char) c;
                } else {
                    answer = "";
                    if (is1234(c)) {
                        if (newc < 758) {
                            newc = newc * 10 + c;
                        } else {
                            newc = c;
                        }
                    }
                    if (c == 41) {
                        if (newc != 758) {
                            answer = answer + (char) newc + (char) c;
                            writeQuestion = true;
                            newc = 758;
                            questions.add(question);
                            question = "";
                        }
                    }
                    question = question + (char) c;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        map.put("Question", questions);
        map.put("Answer", answers);
        return map;
    }

    private boolean is1234(int c) {
        if (c > 47) {
            if (c < 58) {
                return true;
            }
        }
        return false;
    }

    private boolean isABCD(int c) {
        if (c > 64) {
            if (c < 69) {
                return true;
            }
            if (c < 101) {
                if (c > 96) {
                    return true;
                }
            }
        }
        return false;
    }
}
