package part1;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class NewParse {
    private Map<Integer, Character> map = new HashMap<>();
    private int numQuestions = 0;
    private int indexx = 0;

    public StringBuffer parse(String file) {
        StringBuffer sb = new StringBuffer();
        try (FileReader fr = new FileReader(file)) {
            int c;
            while ((c = fr.read()) != -1) {
                sb.append((char) c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb;
    }

    public Tests questions(StringBuffer sb, int numberOfQuestions) {
        numQuestions = numberOfQuestions;
        String s;
        String str;
        List<String> list = new ArrayList<>();
        str = sb.toString();
        sb = containsString("A)", str, sb);
        sb = containsString("B)", str, sb);
        sb = containsString("C)", str, sb);
        sb = containsString("D)", str, sb);
        StringBuffer newsb = new StringBuffer();
        newsb.append(sb);
        Map<Integer,List<String>> answers = answers(newsb);
        //System.out.println(answers);
        for (int i = 1; i < (numberOfQuestions + 1); i++) {
            //System.out.println(sb);
            //System.out.println(sb.indexOf(i + "."));
            //System.out.println(sb.indexOf("a)"));
            System.out.println(i);
            s = sb.substring(sb.indexOf(i + "."), sb.indexOf("a)"));
            sb.delete(1, sb.indexOf("d)"));
            sb.deleteCharAt(1);
            //System.out.println(s);
            list.add(s);
        }
        Tests tests = new Tests();
        tests.setQ(list);
        tests.setA(answers);
        return tests;
    }

    public StringBuffer containsString(String s, String line, StringBuffer sb) {

        while (line.contains(s)) {
            int delA = line.indexOf(s);
            //System.out.println(number(numQuestions, sb, s));
            map.put(number(numQuestions, sb, s), s.charAt(0));
            String news = s.toLowerCase();
            sb.replace(delA, (delA + 2), news);
            //System.out.println(sb);
            line = sb.toString();
        }
        //System.out.println(map);
        //System.out.println(sb);
        return sb;
    }

    public int number(int number, StringBuffer sb, String s) {
        for (; number > 0; number--) {
            if (sb.substring(sb.indexOf(number + ".")).contains(s)) {
                if (!map.containsKey(number)){
                    return number;
                }
            }
        }
        return 0;
    }

    public Map<Integer,List<String>> answers(StringBuffer sb) {
        Map<Integer,List<String>> map = new HashMap<>();
        String s;
        List<String> letters = new ArrayList<>();
        letters.add("a)");
        letters.add("b)");
        letters.add("c)");
        letters.add("d)");
        int itter = 0;
        int index;
        while (itter < numQuestions) {
            List<String> list = new ArrayList<>();
            for (int i = 0; i < 4; i++) {
                String letter = letters.get(i);
                String next;
                if ((itter + 2) > numQuestions && i == 3) {
                    index = sb.length() - 1;
                } else if (i == (letters.size() - 1)) {
                    next = (itter + 2) + ".";
                    index = sb.indexOf(next);
                } else {
                    next = letters.get(i + 1);
                    index = sb.indexOf(next);
                }
                //System.out.println(sb.indexOf(letter)+"//"+ sb.indexOf(next));
                s = sb.substring(sb.indexOf(letter), index);
                sb.delete(1, index);
                //System.out.println(s);
                list.add(s);

            }
            //System.out.println(list);
            map.put(itter+1,list);
            itter = itter + 1;
        }
        return map;
    }

    public Map<Integer, Character> getMap() {
        return map;
    }
}
