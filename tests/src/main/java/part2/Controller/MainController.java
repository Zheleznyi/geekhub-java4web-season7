package part2.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class MainController {





    @RequestMapping(value = "login")
    public String login(Model model, HttpServletRequest req, @RequestParam(required = false)String log){
        req.getSession().setAttribute("login",log);
        model.addAttribute("message", req.getSession().getAttribute("login"));
        if (req.getSession().getAttribute("login")!=null){
            return "main";
        }
        return "login";
    }
}
