package part2.Controller;

import com.sun.media.sound.InvalidDataException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import part1.NewParse;
import part1.ParseObj;
import part1.Tests;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class FileUploadController {

    @RequestMapping(value = "/uploadfile")
    public String provideUploadInfo(Model model, HttpServletRequest req) {
        if (req.getSession().getAttribute("login") == null) {
            return "login";
        }
        model.addAttribute("message", req.getSession().getAttribute("login"));
        return "main";
    }

    @RequestMapping(value = "/upload")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,HttpServletRequest req, Model model) throws Exception {
        String name = "D:/Documents/geek/downloads/";
        if (!file.isEmpty()) {
            byte[] bytes = file.getBytes();
            BufferedOutputStream stream =
                    new BufferedOutputStream(new FileOutputStream(new File(name + "test.txt")));
            stream.write(bytes);
            stream.close();
            NewParse newParse = new NewParse();
            Tests tests = newParse.questions(newParse.parse(name+"test.txt"),25);
            //System.out.println(newParse.getMap());
            req.getSession().setAttribute("answers",newParse.getMap());
            model.addAttribute("questions",tests.getQ());
            model.addAttribute("answers",tests.getA());
            return "test";
        } else {
            throw new InvalidDataException("File is empty");
        }
    }

    @RequestMapping(value = "home")
    public String home(Model model, HttpServletRequest req, @RequestParam Map<String,String> allRequestParams){
        if (req.getSession().getAttribute("login")==null){
            return "login";
        }
        model.addAttribute("message", req.getSession().getAttribute("login"));
        Map<Integer, Character> map = (Map<Integer, Character>) req.getSession().getAttribute("answers");
        List<Integer> list = new ArrayList<>();
        int trueAnswers=0;
        for (int i = 0; i<map.size(); i++){
            //System.out.println(map.get(i+1));
            String s = map.get(i+1).toString().toLowerCase();
            if (s.equals(allRequestParams.get(i+1+""))){
                trueAnswers = trueAnswers + 1;
            }else {
                list.add(i+1);
            }
        }
        model.addAttribute("answers",trueAnswers);
        model.addAttribute("list",list);
        return "home";
    }

}
