package work;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Demo {
    public static void main(String[] args) {
        //CreateDB ex = new CreateDB();
        //ex.dataBase();
        //Add add = new Add();
        //add.add();
        WorkWithDB exercise2 = new WorkWithDB();
        try {
            String url = exercise2.voteUrl();
            String user = exercise2.voteUser();
            String pass = exercise2.votePassword();
            exercise2.db(url, user, pass);
            ResultSet set = exercise2.select(url, user, pass);
            System.out.println(set.getMetaData().getColumnName(1) + "  " + set.getMetaData().getColumnName(2) + "     "
                    + set.getMetaData().getColumnName(3));
            while (set.next()) {
                String s = set.getString("Name");
                int length = s.length();
                for (int i = 9; i > length; i--) {
                    s = s + " ";
                }
                System.out.print("  " + set.getInt("Number") + "     ");
                System.out.print(s);
                System.out.println(set.getInt("Age") + "     ");

            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
