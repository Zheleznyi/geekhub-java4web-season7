package work;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateDB {
    public void dataBase() {
        try (Connection connection = DriverManager.getConnection(

                "jdbc:postgresql://localhost:5432/moskalov",
                "postgres", "Fritz123"
        )
        ) {
            Statement statement = connection.createStatement();
            String sql = "CREATE TABLE users( Number INT NOT NULL ," +
                    " Name VARCHAR(255) NOT NULL ," +
                    "Age INT NOT NULL )";
            statement.executeUpdate(sql);

        } catch (SQLException e) {
            System.out.println("This database has already been created");
        }
    }
}
