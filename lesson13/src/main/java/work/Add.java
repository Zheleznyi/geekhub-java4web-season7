package work;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Add {
    public void add() {
        try (Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/moskalov",
                "postgres", "Fritz123")) {
            Statement statement = connection.createStatement();
            String sql = "INSERT INTO users VALUES (2,'fox',12)";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
