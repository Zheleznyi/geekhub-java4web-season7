package work;

import java.sql.*;
import java.util.Scanner;

public class WorkWithDB {
    private Scanner in = new Scanner(System.in);

    public String voteUrl() {
        System.out.println("Please input url DB: ");
        return in.next();
    }

    public String voteUser() {
        System.out.println("Please input com.user: ");
        return in.next();
    }

    public String votePassword() {
        System.out.println("Please input password: ");
        return in.next();
    }

    public String voteSql() {
        System.out.println("Please input sqlquery: ");
        return in.nextLine();
    }

    public ResultSet select(String url, String user, String password) throws SQLException {
        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            Statement statement = connection.createStatement();
            String sql = "SELECT number,name,age FROM users";
            ResultSet set = statement.executeQuery(sql);
            return set;
        }
    }

    public void db(String url, String user, String password) throws SQLException {
        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            Statement statement = connection.createStatement();
            String s = voteSql();
            statement.executeUpdate(voteSql());
        }
    }


}
