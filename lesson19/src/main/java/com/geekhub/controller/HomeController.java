package com.geekhub.controller;

import com.geekhub.service.Db;
import com.geekhub.service.UserService;
import com.geekhub.service.loggerService.LoggerService;
import com.geekhub.user.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.SQLException;
import java.util.List;

@Controller
public class HomeController {
    @Value("${property.a}") private String prop;
    private UserService us;

    @GetMapping(value = "home")
    public String home(@RequestParam String action ,@RequestParam String name, @RequestParam String value, Model model){
        List<User> list = null;
        String result = "home";
        result = test(name, value);
        if (!result.equals("home")){
            return result;
        }
        try {
            us = new UserService(new Db());
            User user = us.createUser(name,value);
            result = choose(action, user);
            list = us.getusers();
        } catch (SQLException e) {
            LoggerService.out("SqlException throws");
        }
        model.addAttribute("prop", prop);
        model.addAttribute("users", list);
        return result;
    }

    @GetMapping(value = "main")
    public String main(Model model){
        model.addAttribute("prop",prop);
        return "main";
    }

    private String choose(String action, User user) throws SQLException {
        if (action.equals("add")){
            us.add(user);
        } else if (action.equals("invalidate")){
            us.invalidate();
        }else if (action.equals("update")) {
            us.update(user);
        } else if (action.equals("delete")){
            us.delete(user);
        } else {
            return "freelineerror";
        }
        return "home";
    }

    private String test(String name, String value){
        if (name.equals("")||value.equals("")){
            return "emptyerror";
        }
        return "home";
    }

}
