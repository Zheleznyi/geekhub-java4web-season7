package com.geekhub.user;

public class User {
    private final String name;
    private final String value;


    public User(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}