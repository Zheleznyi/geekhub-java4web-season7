package com.geekhub.service;

import com.geekhub.user.User;

import java.awt.image.RescaleOp;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserService {
    private final Db db;

    public UserService(Db db) throws SQLException {
        this.db = db;
        db.dBUpdate("CREATE TABLE IF NOT EXISTS tableSpring(" +
                "Name VARCHAR(255) NOT NULL," +
                "Value VARCHAR(255) NOT NULL)");
    }

    public User createUser(String name, String value) {
        return new User(name, value);
    }

    public void add(User user) throws SQLException {
        db.dBUpdate("INSERT INTO tableSpring VALUES('" +
                user.getName() + "','" +
                user.getValue() + "')");
    }

    public void delete(User user) throws SQLException {
        db.dBUpdate("DELETE FROM tableSpring WHERE name = '"+user.getName()+"'AND " +
                "value ='"+user.getValue()+"'");
    }

    public void invalidate() throws SQLException {
        db.dBUpdate("DELETE FROM tableSpring");
    }

    public void update(User user) throws SQLException {
        db.dBUpdate("UPDATE tableSpring SET value = '"+ user.getValue() + "' WHERE name = '" + user.getName()+"'");
    }

    public List<User> getusers() throws SQLException {
        ResultSet set = db.select("SELECT name,value FROM tableSpring");
        List<User> users = new ArrayList<>();
        while (set.next()) {
            User user = createUser(set.getString("name"), set.getString("value"));
            users.add(user);
        }
        return users;
    }

}
