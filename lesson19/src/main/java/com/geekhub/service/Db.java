package com.geekhub.service;

import java.sql.*;

public class Db {

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/moskalov",
                "postgres", "Fritz123"
        );
    }

    public void dBUpdate(String sql) throws SQLException {
        try (Connection connection = createConnection()){
            Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
        }
    }

    public ResultSet select(String sql) throws SQLException {
        try(Connection connection = createConnection()){
            Statement statement = connection.createStatement();
            return statement.executeQuery(sql);
        }
    }


}
