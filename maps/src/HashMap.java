import java.util.Map;
import java.util.Objects;

public class HashMap<K, V> {

    static class Cell<K, V> implements Map.Entry<K, V> {
        final int hash;
        final K key;
        V value;
        Cell<K, V> next;

        Cell(int hash, K key, V value, Cell<K, V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }

        @Override
        public String toString() {
            return key + " = " + value;
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(key) ^ Objects.hashCode(value);
        }

        public final boolean equals(Object o) {
            if (o == this)
                return true;
            if (o instanceof Map.Entry) {
                Map.Entry<?, ?> e = (Map.Entry<?, ?>) o;
                if (Objects.equals(key, e.getKey()) &&
                        Objects.equals(value, e.getValue()))
                    return true;
            }
            return false;
        }
    }

    Cell<K, V>[] nodes;
    int size;
    static final int DEFAULT_INITIAL_CAPACITY = 1 << 4;
    static final int MAXIMUM_CAPACITY = 1 << 30;


    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    final Cell<K, V> getCell(int hash, Object key) {
        Cell<K, V>[] cells;
        Cell<K, V> first, e;
        int n;
        K k;
        if ((cells = nodes) != null && (n = cells.length) > 0 && (first = cells[(n - 1) & hash]) != null) {
            if (first.hash == hash && ((k = first.key) == key || (key != null && key.equals(k)))) {
                return first;
            }
            if ((e = first.next) != null) {
                do {
                    if (e.hash == hash && ((k = e.key) == key || (key != null && key.equals(k))))
                        return e;
                } while ((e = e.next) != null);
            }
        }
        return null;
    }

    public V get(Object key) {
        Cell<K, V> e;
        return (e = getCell(hash(key), key)) == null ? null : e.value;
    }

    static int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    public boolean containskey(Object key) {
        return getCell(hash(key), key) != null;
    }

    public V putValue(int hash, K key, V value, boolean onlyIfAbsent, boolean evict) {
        Cell<K, V>[] cells;
        Cell<K, V> e;
        int n, i;
        if ((cells = nodes) == null || (n = cells.length) == 0) {
            n = (cells = resize()).length;
        }
        if ((e = cells[i = (n - 1) & hash]) == null) {
            cells[i] = newCell(hash, key, value, null);
        }
        return null;
    }

    private Cell<K, V> newCell(int hash, K key, V value, Cell<K, V> next){
        return new Cell<>(hash, key, value, next);
    }

    final Cell<K, V>[] resize() {
        Cell<K, V>[] oldTab = nodes;
        int oldCapacity = (oldTab == null) ? 0 : oldTab.length;
        int newCap = 0;
        if (oldCapacity > 0) {
            if (oldCapacity >= MAXIMUM_CAPACITY) {
                return oldTab;
            } else {
                newCap = oldCapacity << 1;
            }
        }
        Cell<K, V>[] newTab = (Cell<K, V>[]) new Cell[newCap];
        nodes = newTab;
        return newTab;
    }


//    private K[] keys;
//    List<V> values;
//
//    public HashMap() {
//    }
//
//    public V put(K key, V value){
//        keys[0] = key;
//        System.out.println(keys.length);
//        return null;
//
//    }
}
